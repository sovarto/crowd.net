﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Tests
{
    [TestClass]
    public class GroupTests
    {
        [TestMethod]
        [ExpectedException(typeof(CrowdGroupNotFoundException))]
        public void GetGroup_WithInvalidName_ShouldNotReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            cwd.GetGroup(TestGlobal.Test_NewGroupName);

            //  Assert is handled by ExpectedException

        }

        [TestMethod]
        public void GetGroup_WithValidName_ShouldReturnGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            Group response = cwd.GetGroup(TestGlobal.Test_GroupName);

            //  Assert
            Assert.AreEqual(TestGlobal.Test_GroupName, response.Name);

        }

        [TestMethod]
        public void AddGroup_WithUniqueName_ShouldReturnGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group newGroup = new Group { Name = TestGlobal.Test_NewGroupName, Description = string.Format("{0} description for test case", TestGlobal.Test_NewGroupName), active = true, type = "GROUP" };

            //  Act
            cwd.AddGroup(newGroup);
            Group response = cwd.GetGroup(newGroup.Name);

            //  Assert
            Assert.AreEqual(response.Name, newGroup.Name);
            Assert.AreEqual(response.Description, newGroup.Description);
            Assert.AreEqual(response.type, newGroup.type);
            Assert.AreEqual(response.active, newGroup.active);

        }

        [TestMethod]
        [ExpectedException(typeof(CrowdInvalidGroupException))]
        public void AddGroup_WithDuplicateName_ShouldReturnBadRequestError()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group existingGroup = cwd.GetGroup(TestGlobal.Test_NewGroupName);

            //  Act
            cwd.AddGroup(existingGroup);

            // Assert is handled by ExpectedException

        }

        [TestMethod]
        public void UpdateGroup_WithExisitingName_ShouldReturnGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group existingGroup = cwd.GetGroup(TestGlobal.Test_NewGroupName);
            existingGroup.Description += " UPDATED";

            //  Act
            Group response = cwd.UpdateGroup(existingGroup);

            //  Assert
            Assert.AreEqual(response.Name, existingGroup.Name);
            Assert.AreEqual(response.Description, existingGroup.Description);
            Assert.AreEqual(response.type, existingGroup.type);
            Assert.AreEqual(response.active, existingGroup.active);

        }

        [TestMethod]
        public void StoreGroupAttributes_WithValidName_ShouldReturnGroupWithAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Crowd.Rest.Client.Entity.Attribute attribute = new Crowd.Rest.Client.Entity.Attribute { name = TestGlobal.Test_NewAttributeName, values = new List<string> { TestGlobal.Test_NewAttributeValue } };
            AttributeList attributes = new AttributeList { Attributes = new List<Crowd.Rest.Client.Entity.Attribute> { attribute } };

            //  Act
            cwd.StoreGroupAttributes(TestGlobal.Test_NewGroupName, attributes);

            //  Assert
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetGroupAttributes(TestGlobal.Test_NewGroupName);
            Assert.AreEqual(TestGlobal.Test_NewAttributeValue, response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName).values.FirstOrDefault());
        }

        [TestMethod]
        public void GetGroupWithAttributes_WithValidName_ShouldReturnGroupWithAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            GroupWithAttributes response = cwd.GetGroupWithAttributes(TestGlobal.Test_NewGroupName);

            //  Assert
            Assert.AreEqual(TestGlobal.Test_NewGroupName, response.Name);
            Assert.AreEqual(TestGlobal.Test_NewAttributeName, response.Attributes.Attributes.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName).name);
        }
        
        [TestMethod]
        public void GetGroupAttributes_WithValidName_ShouldReturnAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetGroupAttributes(TestGlobal.Test_NewGroupName);

            //  Assert
            Assert.IsNotNull(response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName));
        }
        [TestMethod]
        public void AddGroupToGroup_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.AddGroupToGroup(TestGlobal.Test_NewGroupName, TestGlobal.Test_GroupName);
            }
            catch
            {
                //  Assert
                Assert.Fail("Adding a group to a group should not fail");
            }
        }
        [TestMethod]
        public void GetParentGroupsForGroup_ShouldReturnGroups()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<Group> response = cwd.GetParentGroupsForGroup(TestGlobal.Test_NewGroupName);

            //  Assert
            Assert.IsTrue(response.Count >= 1, "The user count was not greater than or equal to 1");
        }
        // Test GetNamesOfParentGroupsForGroup
        [TestMethod]
        public void GetNamesParentGroupsForGroup_ShouldReturnGroupNames()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<string> response = cwd.GetNamesOfParentGroupsForGroup(TestGlobal.Test_NewGroupName);

            //  Assert
            Assert.IsTrue(response.Count >= 1, "The user count was not greater than or equal to 1");
        }

        // Test GetParentGroupsForNestedGroup

        // Test GetNamesParentGroupsForNestedGroup

        [TestMethod]
        public void RemoveGroupFromGroup_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.RemoveGroupFromGroup(TestGlobal.Test_NewGroupName, TestGlobal.Test_GroupName);
            }
            catch
            {
                //  Assert
                Assert.Fail("Removing a group to a group should not fail");
            }
        }
        [TestMethod]
        public void DeleteGroupAttributes_WithExistingName_ShouldDeleteGroupAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group existingGroup = cwd.GetGroup(TestGlobal.Test_NewGroupName);

            //  Act
            cwd.RemoveGroupAttributes(existingGroup.Name, TestGlobal.Test_NewAttributeName);

            //  Assert
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetGroupAttributes(TestGlobal.Test_NewGroupName);
            Assert.IsNull(response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName));
        }

        [TestMethod]
        public void DeleteGroup_WithExistingName_ShouldDeleteGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group existingGroup = cwd.GetGroup(TestGlobal.Test_NewGroupName);

            //  Act
            try
            {
                cwd.RemoveGroup(existingGroup.Name);
            }
            catch (Exception ex)
            {
                //  Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(CrowdGroupNotFoundException))]
        public void DeleteGroup_WithInvalidName_ShouldReturnNotFoundException()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Group existingGroup = cwd.GetGroup(TestGlobal.Test_NewGroupName);

            //  Act
            cwd.RemoveGroup(existingGroup.Name);

            //  Assert
            Group response = cwd.GetGroup(TestGlobal.Test_NewGroupName);
            Assert.IsNull(response);
        }

        [TestMethod]
        public void GetUsersOfGroup_WithValidName_ShouldReturnUsers()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<User> response = cwd.GetUsersOfGroup(TestGlobal.Test_GroupName);

            //  Assert
            Assert.IsTrue(response.Count >= 1, "The user count was not greater than or equal to 1");

        }
    }
}
