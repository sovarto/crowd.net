﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Security;
using Crowd.Rest.Client;
using System.Configuration.Provider;

namespace Crowd.Tests
{
    [TestClass]
    public class RoleTests: RoleTestBase
    {

        [TestMethod]
        public void RoleProvider_CanCreateRole()
        {
            try
            {
                //  Arrange
                this.Initialize();

                // Act
                _provider.CreateRole(_testRole);
            }
            catch (Exception)
            {
                // Assert
                Assert.Fail();
            }
        }

        [TestMethod]
        public void RoleProvider_CanAddUsersToRoles()
        {
            try
            {
                //  Arrange
                this.Initialize();

                // Act
                _provider.AddUsersToRoles(new string[] { TestGlobal.Test_UserName }, new string[] { _testRole });
            }
            catch (Exception)
            {
                // Assert
                Assert.Fail();
            }
        }

        [TestMethod]
        public void RoleProvider_CanFindUsersInRole()
        {
            //  Arrange
            this.Initialize();

            // Act
            string[] response = _provider.FindUsersInRole(_testRole, TestGlobal.Test_UserName);

            // Assert
            Assert.IsFalse(response == null);
            Assert.IsTrue(response.Length > 0);
        }

        [TestMethod]
        public void RoleProvider_CanGetAllRoles()
        {
            //  Arrange
            this.Initialize();

            // Act
            string[] response = _provider.GetAllRoles();

            // Assert
            Assert.IsFalse(response == null);
            Assert.IsTrue(response.Length > 0);
        }

        [TestMethod]
        public void RoleProvider_CanGetRolesForUser()
        {
            //  Arrange
            this.Initialize();

            // Act
            string[] response = _provider.GetRolesForUser(TestGlobal.Test_UserName);

            // Assert
            Assert.IsFalse(response == null);
            Assert.IsTrue(response.Length > 0);
        }

        [TestMethod]
        public void RoleProvider_CanGetUsersInRole()
        {
            //  Arrange
            this.Initialize();

            // Act
            string[] response = _provider.GetUsersInRole(_testRole);

            // Assert
            Assert.IsFalse(response == null);
            Assert.IsTrue(response.Length > 0);
        }

        [TestMethod]
        public void RoleProvider_CanCheckIsUserInRole()
        {
            //  Arrange
            this.Initialize();

            // Act
            bool response = _provider.IsUserInRole(TestGlobal.Test_UserName, _testRole);

            // Assert
            Assert.IsTrue(response);
        }

        [TestMethod]
        [ExpectedException(typeof(ProviderException))]
        public void RoleProvider_CannotDeletePopulatedRole()
        {
            //  Arrange
            this.Initialize();

            // Act
            _provider.DeleteRole(_testRole, true);

            // Assert handled by ExpectedException
        }
        
        [TestMethod]
        public void RoleProvider_CanRemoveUsersFromRoles()
        {
            try
            {
                //  Arrange
                this.Initialize();

                // Act
                _provider.RemoveUsersFromRoles(new string[] { TestGlobal.Test_UserName }, new string[] { _testRole });
            }
            catch (Exception)
            {
                // Assert
                Assert.Fail();
            }
        }

        [TestMethod]
        public void RoleProvider_CanCheckRoleExists()
        {
            //  Arrange
            this.Initialize();

            // Act
            bool response = _provider.RoleExists(_testRole);

            // Assert
            Assert.IsTrue(response);
        }
        
        [TestMethod]
        public void RoleProvider_CanDeleteEmptyRole()
        {
            try
            {
                //  Arrange
                this.Initialize();

                // Act
                _provider.DeleteRole(_testRole, false);
            }
            catch (Exception)
            {
                // Assert
                Assert.Fail();
            }
        }

    }
}
