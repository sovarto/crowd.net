﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;
using System.Collections.Generic;

namespace Crowd.Tests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        [ExpectedException(typeof(CrowdApplicationPermissionException))]
        public void GetUser_WithInvalidAppName_ShouldThrowApplicationNotFoundException()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName + "XXX", TestGlobal.Test_ApplicationPassword);

            //  Act
            cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Assert is handled by ExpectedException

        }

        [TestMethod]
        [ExpectedException(typeof(CrowdApplicationPermissionException))]
        public void GetUser_WithInvalidAppPassword_ShouldThrowApplicationPermissionException()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword + "XXX");

            //  Act
            cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Assert is handled by ExpectedException

        }

        [TestMethod]
        [ExpectedException(typeof(CrowdUserNotFoundException))]
        public void GetUser_WithInvalidName_ShouldNotReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Assert is handled by ExpectedException

        }

        [TestMethod]
        public void CreateUser_WithUniqueName_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            User newUser = new User { Name = TestGlobal.Test_NewUserName, DisplayName = "Test 1234", FirstName = "Test", LastName = "User", Password = new Password { value = "P455w0rd!" }, Email = "test@example.com", IsActive = true };

            //  Act
            cwd.AddUser(newUser);
            User response = cwd.GetUser(newUser.Name);

            //  Assert
            Assert.AreEqual(response.Name, newUser.Name);
            Assert.AreEqual(response.Email, newUser.Email);
            Assert.AreEqual(response.FirstName, newUser.FirstName);
            Assert.AreEqual(response.LastName, newUser.LastName);
            Assert.AreEqual(response.DisplayName, newUser.DisplayName);
            Assert.AreEqual(response.IsActive, newUser.IsActive);
        }

        [TestMethod]
        public void UpdateUser_WithExisitingName_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            User existingUser = cwd.GetUser(TestGlobal.Test_NewUserName);
            existingUser.Email = "updated.test@example.com";
            existingUser.FirstName += "A";
            existingUser.LastName += "B";
            existingUser.IsActive = !existingUser.IsActive;
            existingUser.DisplayName += " UPDATED";

            //  Act
            cwd.UpdateUser(existingUser);

            //  Assert
            User response = cwd.GetUser(existingUser.Name);
            Assert.AreEqual(response.Name, existingUser.Name);
            Assert.AreEqual(response.Email, existingUser.Email);
            Assert.AreEqual(response.FirstName, existingUser.FirstName);
            Assert.AreEqual(response.LastName, existingUser.LastName);
            Assert.AreEqual(response.DisplayName, existingUser.DisplayName);
            Assert.AreEqual(response.IsActive, existingUser.IsActive);

        }

        [TestMethod]
        public void UpdateUserPassword_WithValidName_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.UpdateUserPassword(TestGlobal.Test_NewUserName, "Upd4t3eP455w0rd!");
            }
            catch (Exception ex)
            {
                //  Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void StoreUserAttributes_WithValidName_ShouldReturnUserWithAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            Crowd.Rest.Client.Entity.Attribute attribute = new Crowd.Rest.Client.Entity.Attribute { name = TestGlobal.Test_NewAttributeName, values = new List<string> { TestGlobal.Test_NewAttributeValue } };
            AttributeList attributes = new AttributeList { Attributes = new List<Crowd.Rest.Client.Entity.Attribute> { attribute } };

            //  Act
            cwd.StoreUserAttributes(TestGlobal.Test_NewUserName, attributes);

            //  Assert
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetUserAttributes(TestGlobal.Test_NewUserName);
            Assert.AreEqual(TestGlobal.Test_NewAttributeValue, response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName).values.FirstOrDefault());
        }

        [TestMethod]
        public void GetUserWithAttributes_WithValidName_ShouldReturnUserWithAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            UserWithAttributes response = cwd.GetUserWithAttributes(TestGlobal.Test_NewUserName);

            //  Assert
            Assert.AreEqual(TestGlobal.Test_NewUserName, response.Name);
            Assert.AreEqual(TestGlobal.Test_NewAttributeName, response.Attributes.Attributes.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName).name);
        }

        [TestMethod]
        public void GetUserAttributes_WithValidName_ShouldReturnAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetUserAttributes(TestGlobal.Test_NewUserName);

            //  Assert
            Assert.IsNotNull(response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName));
        }

        [TestMethod]
        public void DeleteUserAttributes_WithExistingName_ShouldDeleteUserAttributes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            User existingUser = cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Act
            cwd.RemoveUserAttributes(existingUser.Name, TestGlobal.Test_NewAttributeName);

            //  Assert
            List<Crowd.Rest.Client.Entity.Attribute> response = cwd.GetUserAttributes(TestGlobal.Test_NewUserName);
            Assert.IsNull(response.FirstOrDefault(x => x.name == TestGlobal.Test_NewAttributeName));
        }

        [TestMethod]
        public void AddUserToGroup_WithValidName_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.AddUserToGroup(TestGlobal.Test_NewUserName, TestGlobal.Test_GroupName);
            }
            catch (Exception ex)
            {
                //  Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetGroupsForUser_WithValidName_ShouldReturnGroups()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<Group> response = cwd.GetGroupsForUser(TestGlobal.Test_NewUserName);

            //  Assert
            Assert.IsTrue(response.Count >= 1, "The attribute was not greater than or equal to 1");

        }
        [TestMethod]
        public void IsUserDirectGroupMember_WithValidName_ShouldReturnTrue()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            bool response = cwd.IsUserDirectGroupMember(TestGlobal.Test_NewUserName, TestGlobal.Test_GroupName);

            //  Assert
            Assert.IsTrue(response);

        }

        [TestMethod]
        public void RemoveUserFromGroup_WithValidName_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.RemoveUserFromGroup(TestGlobal.Test_NewUserName, TestGlobal.Test_GroupName);
            }
            catch (Exception ex)
            {
                //  Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void DeleteUser_WithExistingName_ShouldDeleteUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            User existingUser = cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Act
            try
            {
                cwd.RemoveUser(existingUser.Name);
            }
            catch (Exception ex)
            {
                //  Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(CrowdUserNotFoundException))]
        public void DeleteUser_WithInvalidName_ShouldReturnNotFoundException()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            User existingUser = cwd.GetUser(TestGlobal.Test_NewUserName);

            //  Act
            cwd.RemoveUser(existingUser.Name);

            //  Assert is handled by ExpectedException

        }

        [TestMethod]
        public void RequestPasswordReset_WithValidName_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.RequestPasswordReset(TestGlobal.Test_UserName);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }
        
        [TestMethod]
        public void RequestUsernames_WithValidEmail_ShouldNotFail()
        {
            try
            {
                //  Arrange
                CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

                //  Act
                cwd.RequestUsernames(TestGlobal.Test_EmailAddress);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

    }
}
