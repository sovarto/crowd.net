﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Tests
{
    [TestClass]
    public class WebhookTests
    {
        private string _endpointUrl = "http://www.example.com/";
        private string _webhookToken = "mytoken";

        [TestMethod]
        public void RegisterWebhook_ShouldReturnId()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            long response = cwd.RegisterWebhook(this._endpointUrl, _webhookToken);

            //  Assert
            Assert.IsTrue(response > 0, "The response was not greater than zero"); ;

        }

        [TestMethod]
        public void GetWebhook_ShouldReturnUrl()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            long webhookId = cwd.RegisterWebhook(this._endpointUrl, _webhookToken);

            //  Act
            string response = cwd.GetWebhook(webhookId);

            //  Assert
            Assert.AreEqual(this._endpointUrl, response);

        }

        [TestMethod]
        public void UnregisterWebhook_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            long webhookId = cwd.RegisterWebhook(this._endpointUrl, _webhookToken);

            try
            {
                //  Act
                cwd.UnregisterWebhook(webhookId);
            }
            catch
            {
                //  Assert
                Assert.Fail();
            }
        }
    
    }
}
