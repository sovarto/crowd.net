﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Crowd")]
[assembly: AssemblyDescription("Crowd.NET is a Microsoft .NET library which provides a simple interface for interacting with the the REST API resources provided by Atlassian Crowd")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Ian Perrin")]
[assembly: AssemblyProduct("Crowd")]
[assembly: AssemblyCopyright("Copyright © 2013-2015 Ian Perrin")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f463131a-a1b6-4a15-b1d7-448be0d8be2d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.3.*")]
[assembly: AssemblyInformationalVersion("1.3.2")]

