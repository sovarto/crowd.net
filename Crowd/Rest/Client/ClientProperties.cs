﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crowd.Rest.Client
{
    /// <summary>
    /// Properties required for the Crowd Client.
    /// Normally the properties are stored in crowd.properties file.
    /// see https://docs.atlassian.com/atlassian-crowd/current/com/atlassian/crowd/service/client/AbstractClientProperties.html
    /// </summary>
    public class ClientProperties
    {
        #region Fields and Properties
        /// <summary>
        /// Gets the name of the application used in application authentication.
        /// </summary>
        /// <value>
        /// The name of the application  used in application authentication.
        /// </value>
        public string ApplicationName { get; private set; }

        /// <summary>
        /// Gets the application password used for authenticating the application.
        /// </summary>
        /// <value>
        /// The application password used for authenticating the application.
        /// </value>
        public string ApplicationPassword { get; private set; }

        /// <summary>
        /// Gets the base URL of the client application.
        /// </summary>
        /// <value>
        /// The base URL of the client application.
        /// <example>http://localhost:8095/crowd</example>
        /// </value>
        public string BaseUrl { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientProperties"/> class.
        /// </summary>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="applicationPassword">The application password.</param>
        public ClientProperties(string baseUrl, string applicationName, string applicationPassword)
        {
            this.ApplicationName = applicationName;
            this.ApplicationPassword = applicationPassword;
            this.BaseUrl = baseUrl;
        }
        #endregion

    }
}
