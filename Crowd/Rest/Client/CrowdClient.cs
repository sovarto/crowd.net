﻿using System;
using System.Net;
using System.Text;
using System.Web;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;
using System.Reflection;

namespace Crowd.Rest.Client
{
    /// <summary>
    /// Abstract class for a Crowd Client
    /// </summary>
    public abstract partial class CrowdClient
    {

        #region Fields and properties
        /// <summary>
        /// URI for a Crowd REST API resource.
        /// More information here: https://developer.atlassian.com/display/CROWDDEV/Using+the+Crowd+REST+APIs#UsingtheCrowdRESTAPIs-RESTResourcesandURIStructure
        /// </summary>
        private const string _apiPath = "{0}/rest/usermanagement/{1}/{2}";
        /// <summary>
        /// The API version
        /// </summary>
        private string _apiVersion;
        /// <summary>
        /// The client properties used to connect to the Crowd server
        /// </summary>
        private ClientProperties _clientProperties;
        /// <summary>
        /// The cookies issued by the Crowd server
        /// </summary>
        private CookieContainer _cookieJar = new CookieContainer();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestClientManager"/> class for 
        /// interacting with a remote Crowd server.
        /// </summary>
        /// <param name="clientProperties">The crowd properties for the client.</param>
        public CrowdClient(ClientProperties clientProperties, string apiVersion)
        {
            _clientProperties = clientProperties;
            _apiVersion = apiVersion;
        }

        #endregion

        #region API Call

        /// <summary>
        /// Generic API call.  Expects to be able to serialize the results
        /// to the specified type.
        /// </summary>
        /// <typeparam name="T">The specified results type</typeparam>
        /// <param name="apiResource">The API resource.  Example: user</param>
        /// <param name="httpMethod">The HTTP method.  Example: POST</param>
        /// <param name="args">The object that will be serialized as the JSON parameters to the API call</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">API URI not valid - either the Uri Stem or the API resource not specified).</exception>
        /// <exception cref="CrowdRestException"></exception>
        protected virtual T MakeAPICall<T>(string apiResource, string httpMethod, object args = null)
        {
            // Validate inputs
            if (string.IsNullOrEmpty(_clientProperties.BaseUrl))
                throw new ApplicationException("API URI not valid (Uri Stem not specified)");
            if (string.IsNullOrEmpty(apiResource))
                throw new ApplicationException("API URI not valid (API resource not specified)");

            // Construct the full url based on the passed apiResource:
            string fullUrl = _apiPath.Fmt(_clientProperties.BaseUrl, this._apiVersion, apiResource);

            //  Initialize the results to return:
            T results = default(T);
            try
            {
                //  Call the API with the passed arguments:
                Action<HttpWebRequest> httpWebRequestFilter = new Action<HttpWebRequest>(this.HttpWebRequestFilter);
                Action<HttpWebResponse> httpWebResponseFilter = new Action<HttpWebResponse>(this.HttpWebResponseFilter);
                switch (httpMethod.ToUpper())
                {
                    case "PUT":
                        results = fullUrl.PutJsonToUrl(args, httpWebRequestFilter, httpWebResponseFilter).Trim().FromJson<T>();
                        break;
                    case "DELETE":
                        results = fullUrl.DeleteFromUrl("application/json", httpWebRequestFilter, httpWebResponseFilter).Trim().FromJson<T>();
                        break;
                    case "POST":
                        // HACK: Temporary solution to address the fact that search resource in the Crowd REST API does not accept JSON or CQL
                        if (apiResource.StartsWith("search"))
                        {
                            results = fullUrl.PostXmlToUrl(args, httpWebRequestFilter, httpWebResponseFilter).Trim().FromJson<T>();
                        }
                        else
                        {
                            results = fullUrl.PostJsonToUrl(args, httpWebRequestFilter, httpWebResponseFilter).Trim().FromJson<T>();
                        }
                        break;
                    case "GET":
                        results = fullUrl.GetJsonFromUrl(httpWebRequestFilter, httpWebResponseFilter).Trim().FromJson<T>();
                        break;
                    default:
                        break;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    HttpWebResponse response = ((HttpWebResponse)ex.Response);
                    HttpStatusCode statusCode = response.StatusCode;
                    Error errorResponse = null;
                    switch (statusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            errorResponse = new Error { Reason = Error.ErrorReason.APPLICATION_PERMISSION_DENIED, Message = ex.GetResponseBody() };
                            break;
                        case HttpStatusCode.Forbidden:
                            errorResponse = new Error { Reason = Error.ErrorReason.APPLICATION_ACCESS_DENIED, Message = ex.GetResponseBody() };
                            break;
                        default:
                            errorResponse = ex.GetResponseBody().FromJson<Error>();
                            break;
                    }
                    throw new CrowdRestException(errorResponse.Message, ex, errorResponse, statusCode);
                }
                // If we got this far...
                throw new CrowdRestException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetResponseBody() ?? ex.Message;
                //  Throw a new exception based on this information:
                throw new CrowdRestException(errorMessage, ex);
            }
            //  Return the results
            return results;
        }

        private void HttpWebRequestFilter(HttpWebRequest request)
        {
            // Add Authorization Headers
            string authInfo = this._clientProperties.ApplicationName + ":" + this._clientProperties.ApplicationPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
            // Add User-Agent Header
            var assembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(assembly.FullName);
            var assemblyVersion = assemblyName.Version;
            request.UserAgent = string.Format("Crowd.NET/{0} ({1}; .NET CLR {2})", assemblyVersion, Environment.OSVersion.ToString(), Environment.Version.ToString());
            // Set CookieContainer 
            request.CookieContainer = _cookieJar;
        }
        private void HttpWebResponseFilter(HttpWebResponse response)
        {
            if (response.Cookies != null && response.Cookies.Count > 0)
                _cookieJar.Add(response.Cookies);
        }

        #endregion

        #region Error Handlers

        /// <summary>
        /// Throws a CrowdUserNotFoundException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="username">The username of the user that could not be found</param>
        /// <exception cref="CrowdUserNotFoundException">if the method handled the error</exception>
        protected static void HandleUserNotFound(Error error, string username)
        {
            if (error != null && error.Reason == Error.ErrorReason.USER_NOT_FOUND)
            {
                throw new CrowdUserNotFoundException(username);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidAuthenticationException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="username">The username of the user that failed authentication.</param>
        /// <exception cref="CrowdInvalidAuthenticationException">if the method handled the error</exception>
        protected static void HandleInvalidUserAuthentication(Error error, string username)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_USER_AUTHENTICATION)
            {
                throw CrowdInvalidAuthenticationException.NewInstanceWithName(username);
            }
        }

        /// <summary>
        /// Throws a CrowdGroupNotFoundException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="groupName">The name of the group that could not be found.</param>
        /// <exception cref="CrowdGroupNotFoundException">if the method handled the error</exception>
        protected static void HandleGroupNotFound(Error error, string groupName)
        {
            if (error != null && error.Reason == Error.ErrorReason.GROUP_NOT_FOUND)
            {
                throw new CrowdGroupNotFoundException(groupName);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidUserException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="user">The invalid user.</param>
        /// <exception cref="CrowdInvalidUserException">if the method handled the error</exception>
        protected static void HandleInvalidUser(Error error, User user)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_USER)
            {
                throw new CrowdInvalidUserException(user, error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidCredentialException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdInvalidCredentialException">if the method handled the error</exception>
        protected static void HandleInvalidCredential(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_CREDENTIAL)
            {
                throw new CrowdInvalidCredentialException(error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidGroupException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="group">The invalid group.</param>
        /// <exception cref="CrowdInvalidGroupException">if the method handled the error</exception>
        protected static void HandleInvalidGroup(Error error, Group group)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_GROUP)
            {
                throw new CrowdInvalidGroupException(group, error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdMembershipNotFoundException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="childName">Name of the child.</param>
        /// <param name="parentName">Name of the parent.</param>
        /// <exception cref="CrowdMembershipNotFoundException">if the method handled the error.</exception>
        protected static void HandleMembershipNotFound(Error error, string childName, string parentName)
        {
            if (error != null && error.Reason == Error.ErrorReason.MEMBERSHIP_NOT_FOUND)
            {
                throw new CrowdMembershipNotFoundException(childName, parentName);
            }
        }

        /// <summary>
        /// Throws a CrowdMembershipAlreadyExistsException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="childEntity">The child entity name.</param>
        /// <param name="parentEntity">The parent entity name.</param>
        /// <exception cref="CrowdMembershipAlreadyExistsException">if the method handled the error</exception>
        protected static void HandleMembershipAlreadyExists(Error error, string childEntity, string parentEntity)
        {
            if (error != null && error.Reason == Error.ErrorReason.MEMBERSHIP_ALREADY_EXISTS)
            {
                throw new CrowdMembershipAlreadyExistsException(childEntity, parentEntity);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidTokenException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdInvalidTokenException">if the method handled the error</exception>
        protected static void HandleInvalidSsoToken(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_SSO_TOKEN)
            {
                throw new CrowdInvalidTokenException(error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdInactiveAccountException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="username">The username.</param>
        /// <exception cref="CrowdInactiveAccountException">if the method handled the error</exception>
        protected static void HandleInactiveUserAccount(Error error, string username)
        {
            if (error != null && error.Reason == Error.ErrorReason.INACTIVE_ACCOUNT)
            {
                throw new CrowdInactiveAccountException(username);
            }
        }

        /// <summary>
        /// Throws a CrowdExpiredCredentialException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdExpiredCredentialException">if the method handled the error</exception>
        protected static void HandleExpiredUserCredential(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.EXPIRED_CREDENTIAL)
            {
                throw new CrowdExpiredCredentialException(error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdInvalidEmailAddressException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdInvalidEmailAddressException">if the method handled the error.</exception>
        protected static void HandleInvalidEmail(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.INVALID_EMAIL)
            {
                throw new CrowdInvalidEmailAddressException(error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdApplicationAccessDeniedException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdApplicationAccessDeniedException">if the method handled the error</exception>
        protected static void HandleApplicationAccessDenied(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.APPLICATION_ACCESS_DENIED)
            {
                throw new CrowdApplicationAccessDeniedException(error.Message);
            }
        }

        /// <summary>
        /// Throws a CrowdEventTokenExpiredException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <exception cref="CrowdEventTokenExpiredException">if the method handled the error.</exception>
        protected static void HandleEventTokenExpiredException(Error error)
        {
            if (error != null && error.Reason == Error.ErrorReason.EVENT_TOKEN_EXPIRED)
            {
                throw new CrowdEventTokenExpiredException();
            }
        }

        /// <summary>
        /// Throws a CrowdWebhookNotFoundException if the method handled the Error. Otherwise, the method silently exits.
        /// </summary>
        /// <param name="error">The error to handle.</param>
        /// <param name="webhookId">The id of the webhook that could not be found.</param>
        /// <exception cref="CrowdWebhookNotFoundException">if the method handled the error</exception>
        protected static void HandleWebhookNotFound(Error error, long webhookId)
        {
            if (error != null && error.Reason == Error.ErrorReason.WEBHOOK_NOT_FOUND)
            {
                throw new CrowdWebhookNotFoundException(webhookId);
            }
        }
        /// <summary>
        /// Handles the exceptions common across all REST methods. This method should be used after all the other 
        /// CrowdRestException handlers. The current implementation is to throw either an ApplicationAccessDenied, 
        /// ApplicationNotFound, ApplicationPermissionException or OperationFailedException.
        /// </summary>
        /// <param name="exception">The Crowd REST Exception to handle.</param>
        /// <returns>Never actually returns a CrowdException</returns>
        /// <exception cref="CrowdApplicationAccessDeniedException">if the application cannot be accessed from the clients location</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the operation.</exception>
        /// <exception cref="CrowdApplicationNotFoundException">if the application cannot be found.</exception>
        /// <exception cref="CrowdOperationFailedException">if the exception has caused the operation to fail.</exception>
        protected CrowdException HandleCommonExceptions(CrowdRestException exception)
        {
            if (exception.Error != null)
            {
                switch (exception.Error.Reason)
                {
                    case Error.ErrorReason.APPLICATION_ACCESS_DENIED:
                        throw new CrowdApplicationAccessDeniedException(exception.Error.Message);
                    case Error.ErrorReason.APPLICATION_NOT_FOUND:
                        throw new CrowdApplicationNotFoundException(exception.Error.Message);
                    case Error.ErrorReason.APPLICATION_PERMISSION_DENIED:
                        throw new CrowdApplicationPermissionException(exception.Error.Message);
                }
            }
            throw new CrowdOperationFailedException("Internal error from Crowd server: " + exception.Message);
        }

        #endregion

    }

}

