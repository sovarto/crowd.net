﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes an Attribute List.
    /// </summary>
    [DataContract(Name = "attributes", Namespace = "")]
    public class AttributeList
    {
        [DataMember(Name = "link")]
        public Link Link { get; set; }
        [DataMember(Name = "attributes")]
        public List<Attribute> Attributes { get; set; }
    }

}
