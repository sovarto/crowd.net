﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Crowd.Rest.Client.Entity
{
    [DataContract(Name = "authentication-context", Namespace = "")]
    public class AuthenticationContext
    {
        [DataMember(Name = "username")]
        public string UserName { get; set; }
        [DataMember(Name = "password")]
        public string Password { get; set; }
        [DataMember(Name = "validation-factors")]
        public ValidationFactors ValidationFactors { get; set; }
    }
}
