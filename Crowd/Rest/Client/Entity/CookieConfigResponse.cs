using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Cookie Configuration response.
    /// </summary>
    [DataContract(Name = "cookie-config")]
    public class CookieConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "domain")]
        public string Domain
        {
            get;
            set;
        }
        /// <summary>
        /// the .
        /// </summary>
        [DataMember(Name = "secure")]
        public bool Secure
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "name")]
        public string Name
        {
            get;
            set;
        }
    }
}
