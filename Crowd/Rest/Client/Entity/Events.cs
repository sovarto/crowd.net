﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Events entity.
    /// </summary>
    [DataContract(Name = "events", Namespace = "")]
    public class Events
    {
        [DataMember(Name = "newEventToken")]
        public string NewEventToken { get; set; }
        [DataMember(Name = "incrementalSynchronisationAvailable")]
        public bool? IsIncrementalSynchronisationAvailable { get; set; }
    }

}
