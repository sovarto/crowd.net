﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Membership.
    /// </summary>
    [DataContract(Name = "membership")]
    public class GroupMembership
    {
        [DataMember(Name = "group")]
        public string Group { get; set; }
        [DataMember(Name = "users")]
        public List<User> Users { get; set; }
        [DataMember(Name = "groups")]
        public List<Group> Groups { get; set; }
    }
}
