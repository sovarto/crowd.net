﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Group.
    /// </summary>
    [DataContract(Name = "group", Namespace = "")]
    public class GroupWithAttributes: Group
    {
        [DataMember(Name = "attributes")]
        public AttributeList Attributes { get; set; }
    }

}
