﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Group List.
    /// </summary>
    [DataContract(Name = "groups")]
    public class GroupWithAttributesList
    {

        [DataMember(Name = "groups")]
        public List<GroupWithAttributes> Groups { get; set; }
        [DataMember(Name = "expand")]
        public string Expand { get; set; }

    }
}
