﻿using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Password response.
    /// </summary>
    public class Password
    {
        public Link link { get; set; }
        public string value { get; set; }
    }
}
