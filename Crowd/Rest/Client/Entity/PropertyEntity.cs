﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Crowd.Rest.Client.Entity
{
    public enum PropertyType
    {
        STRING,
    }
    [DataContract(Name = "property", Namespace="")]
    public class PropertyEntity
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "type")]
        public PropertyType Type { get; set; }
    }
}
