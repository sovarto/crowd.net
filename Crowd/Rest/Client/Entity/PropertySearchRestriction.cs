﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public enum MatchMode
    {
        /// <summary>
        /// The exactly matches
        /// </summary>
        EXACTLY_MATCHES,
        /// <summary>
        /// The less than
        /// </summary>
        LESS_THAN,
        /// <summary>
        /// The greater than
        /// </summary>
        GREATER_THAN,
        /// <summary>
        /// The contains
        /// </summary>
        CONTAINS,
    }
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "property-search-restriction", Namespace = "")]
    public class PropertySearchRestriction
    {

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        /// <value>
        /// The property.
        /// </value>
        [DataMember(Name = "property")]
        public PropertyEntity Property { get; set; }
        /// <summary>
        /// Gets or sets the match mode.
        /// </summary>
        /// <value>
        /// The match mode.
        /// </value>
        [DataMember(Name = "match-mode")]
        public MatchMode MatchMode { get; set; }
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
