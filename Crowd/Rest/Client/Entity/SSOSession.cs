﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    public class SSOSession
    {
        public string expand { get; set; }
        public Link link { get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
        [DataMember(Name = "user")]
        public User User { get; set; }
        [DataMember(Name = "created-date")]
        public string CreatedDate { get; set; }
        [DataMember(Name = "expiry-date")]
        public string ExpiryDate { get; set; }
    }
}
