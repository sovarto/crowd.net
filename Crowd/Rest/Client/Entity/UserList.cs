﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a User List.
    /// </summary>
    [DataContract]
    public class UserList
    {

        [DataMember(Name = "users")]
        public List<User> Users { get; set; }
        [DataMember(Name = "expand")]
        public string Expand { get; set; }

    }
}
