﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a User response.
    /// </summary>
    [DataContract(Name = "user", Namespace = "")]
    public class UserWithAttributes: User
    {
        [DataMember(Name = "attributes")]
        public AttributeList Attributes { get; set; }
    }
}
