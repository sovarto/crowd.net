﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// A Webhook is an application-provided HTTP endpoint that is pinged by Crowd to 
    /// notify the occurrence of certain events.
    /// </summary>
    [DataContract(Name = "webhook", Namespace = "")]
    public class Webhook
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }
        [DataMember(Name = "endpointUrl")]
        public string EndpointUrl { get; set; }
//        [DataMember(Name = "application")]
//        public object Application { get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
        /// <summary>
        /// Gets or sets the date of the last failed delivery that has not been followed by any successful delivery.
        /// </summary>
        /// <value>
        /// Date of the last failed delivery that has not been followed by any successful delivery. May be null if the last delivery was successful, or if no delivery has been attempted yet (i.e., new Webhooks).
        /// </value>
        [DataMember(Name = "oldestFailureDate")]
        public DateTime? OldestFailureDate { get; set; }
        /// <summary>
        /// The number of consecutive failed attempts to deliver the ping to the Webhook since 
        /// the last successful delivery, or since the Webhook was created.
        /// </summary>
        /// <value>
        /// The number of consecutive failed attempts to deliver the ping to the Webhook since 
        /// the last successful delivery, or since the Webhook was created. May be zero if the 
        /// last delivery was successful, or if the Webhook has just been created.
        /// </value>
        [DataMember(Name = "failuresSinceLastSuccess")]
        public DateTime? FailuresSinceLastSuccess { get; set; }
    }

}
