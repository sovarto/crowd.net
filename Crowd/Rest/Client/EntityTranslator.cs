﻿using Crowd.Rest.Client.Entity;
using System.Collections.Generic;


namespace Crowd.Rest.Client
{
    /// <summary>
    /// Translates between Crowd REST entities.
    /// </summary>
    public static class EntityTranslator
    {
        #region AttributeList Extension Methods
        /// <summary>
        /// Transforms a AttributeList to a list of attributes.
        /// </summary>
        /// <param name="groupList">The AttributeList to transform.</param>
        /// <returns>A list of attributes</returns>
        public static List<Attribute> ToAttributeList(this AttributeList attributeList)
        {
            List<Attribute> attributes = new List<Attribute>(attributeList.Attributes.Count);
            foreach (Attribute attribute in attributeList.Attributes)
            {
                attributes.Add(attribute);
            }
            return attributes;
        }
        #endregion

        #region GroupList Extension Methods
        /// <summary>
        /// Transforms a GroupList to a list of groups.
        /// </summary>
        /// <param name="groupList">The GroupList to transform.</param>
        /// <returns>A list of groups</returns>
        public static List<Group> ToGroupList(this GroupList groupList)
        {
            List<Group> groups = new List<Group>(groupList.Groups.Count);
            foreach (Group group in groupList.Groups)
            {
                groups.Add(group);
            }
            return groups;
        }

        /// <summary>
        /// Transforms a GroupList to a list of group names.
        /// </summary>
        /// <param name="groupList">The GroupList to transform.</param>
        /// <returns>A list of group names</returns>
        public static List<string> ToNameList(this GroupList groupList)
        {
            List<string> names = new List<string>(groupList.Groups.Count);
            foreach (Group group in groupList.Groups)
            {
                names.Add(group.Name);
            }
            return names;
        }

        #endregion

        #region GroupWithAttributesList Extension Methods
        /// <summary>
        /// Transforms a GroupWithAttributesList to a list of groups.
        /// </summary>
        /// <param name="groupWithAttributesList">The GroupWithAttributesList to transform.</param>
        /// <returns>A list of groups with attributes</returns>
        public static List<GroupWithAttributes> ToGroupList(this GroupWithAttributesList groupWithAttributesList)
        {
            List<GroupWithAttributes> groups = new List<GroupWithAttributes>(groupWithAttributesList.Groups.Count);
            foreach (GroupWithAttributes group in groupWithAttributesList.Groups)
            {
                groups.Add(group);
            }
            return groups;
        }

        #endregion

        #region PropertySearchRestriction Extension Methods
        /// <summary>
        /// Transforms a PropertySearchRestriction to a Crowd Query Language.
        /// </summary>
        /// <param name="searchRestriction">The PropertySearchRestriction to transform.</param>
        /// <returns>A list of groups</returns>
        public static string ToCql(this PropertySearchRestriction searchRestriction)
        {

            string operand;
            switch (searchRestriction.MatchMode)
            {
                case MatchMode.LESS_THAN:
                    operand = "<";
                    break;
                case MatchMode.GREATER_THAN:
                    operand = ">";
                    break;
                case MatchMode.CONTAINS:
                case MatchMode.EXACTLY_MATCHES:
                default:
                    operand = "=";
                    break;
            }
            return string.Format("{0}{1}{2}", searchRestriction.Property.Name, operand, searchRestriction.Value);
        }

        #endregion

        #region UserList Extension Methods
        /// <summary>
        /// Transforms a UserList to a list of users.
        /// </summary>
        /// <param name="userList">The UserList to transform.</param>
        /// <returns>A list of user</returns>
        public static List<User> ToUserList(this UserList userList)
        {
            List<User> users = new List<User>(userList.Users.Count);
            foreach (User user in userList.Users)
            {
                users.Add(user);
            }
            return users;
        }

        /// <summary>
        /// Transforms a UserList to a list of user names.
        /// </summary>
        /// <param name="userList">The UserList to transform.</param>
        /// <returns>A list of user names</returns>
        public static List<string> ToNameList(this UserList userList)
        {
            List<string> names = new List<string>(userList.Users.Count);
            foreach (User user in userList.Users)
            {
                names.Add(user.Name);
            }
            return names;
        }

        #endregion
    } 
}
