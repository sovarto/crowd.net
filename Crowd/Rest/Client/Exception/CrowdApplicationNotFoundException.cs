using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when an application is not found.
    /// </summary>
    public class CrowdApplicationNotFoundException : CrowdObjectNotFoundException
    {
        public string ApplicationName { get; private set; }
        public long? Id { get; private set; }

        /// <summary>
        /// Constructs a new application not found exception with an application name.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        public CrowdApplicationNotFoundException(string applicationName)
            : this(applicationName, null)
        {
        }

        /// <summary>
        /// Constructs a new application not found exception with an application name and
        /// cause.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="innerException">the cause (which is saved for later retrieval by the
        ///         {@link #getCause()} method).  (A null value is
        ///         permitted, and indicates that the cause is nonexistent or
        ///         unknown.)</param>
        public CrowdApplicationNotFoundException(string applicationName, Exception innerException)
            : base("Application <" + applicationName + "> does not exist", innerException)
        {
            this.ApplicationName = applicationName;
            this.Id = null;
        }

        /// <summary>
        /// Constructs a new application not found exception with an application id.
        /// </summary>
        /// <param name="id">Id of the application.</param>
        public CrowdApplicationNotFoundException(long id)
            : this(id, null)
        {
        }

        /// <summary>
        /// Constructs a new application not found exception with an application id and
        /// cause.
        /// </summary>
        /// <param name="id">Id of the application.</param>
        /// <param name="innerException">the cause (which is saved for later retrieval by the
        ///         {@link #getCause()} method).  (A <tt>null</tt> value is
        ///         permitted, and indicates that the cause is nonexistent or
        ///         unknown.)</param>
        public CrowdApplicationNotFoundException(long id, Exception innerException)
            : base("Application <" + id + "> does not exist", innerException)
        {
            this.Id = id;
            this.ApplicationName = null;
        }

    }
}