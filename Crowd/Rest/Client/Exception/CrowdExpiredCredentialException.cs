using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the credentials have expired. This exception should only be thrown when the authentication is successful
    /// using the old credentials.
    /// </summary>
    public class CrowdExpiredCredentialException : CrowdFailedAuthenticationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdExpiredCredentialException"/> class.
        /// </summary>
        public CrowdExpiredCredentialException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdExpiredCredentialException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdExpiredCredentialException(Exception innerException)
            : base(innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdExpiredCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdExpiredCredentialException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdExpiredCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdExpiredCredentialException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}