using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the specified group could not be found.
    /// </summary>
    public class CrowdGroupNotFoundException : CrowdObjectNotFoundException
    {
        /// <summary>
        /// Gets the name of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        public string GroupName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdGroupNotFoundException"/> class.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        public CrowdGroupNotFoundException(string groupName)
            : this(groupName, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdGroupNotFoundException"/> class.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdGroupNotFoundException(string groupName, Exception innerException)
            : base("Group <" + groupName + "> does not exist", innerException)
        {
            this.GroupName = groupName;
        }
    }
}