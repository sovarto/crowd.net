using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the account is inactive.
    /// </summary>
    public class CrowdInactiveAccountException : CrowdFailedAuthenticationException
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Constructs a new InvalidAccountException.
        /// <param name="name">name of the account</param>
        /// </summary>
        public CrowdInactiveAccountException(string name)
            : this(name, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInactiveAccountException"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdInactiveAccountException(string name, Exception innerException)
            : base(string.Format("Account with name <{0}> is inactive", name), innerException)
        {
            this.Name = name;
        }

    }
}