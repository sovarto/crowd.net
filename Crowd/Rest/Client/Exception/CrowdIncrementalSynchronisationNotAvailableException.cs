using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// 
    /// </summary>
    public class CrowdIncrementalSynchronisationNotAvailableException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdIncrementalSynchronisationNotAvailableException"/> class.
        /// </summary>
        public CrowdIncrementalSynchronisationNotAvailableException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdIncrementalSynchronisationNotAvailableException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdIncrementalSynchronisationNotAvailableException(string message)
            : base(message)
        {
        }
    }
}