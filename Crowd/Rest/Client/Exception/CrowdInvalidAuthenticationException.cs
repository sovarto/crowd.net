using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the attempted authentication is not valid.
    /// </summary>
    public class CrowdInvalidAuthenticationException : CrowdException
    {
        /// <summary>
        /// Constructs a new <code>CrowdInvalidAuthenticationException</code> with the specified detail message.
        /// <param name="message">detail message</param>
        /// </summary>
        public CrowdInvalidAuthenticationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs a new <code>CrowdInvalidAuthenticationException</code> with the specified detail message and cause.
        /// <param name="message">detail message</param>
        /// <param name="innerException">the inner exception</param>
        /// </summary>
        public CrowdInvalidAuthenticationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Constructs a new <code>CrowdInvalidAuthenticationException</code> with the specified cause.
        /// <param name="innerException">the inner exception</param>
        /// </summary>
        public CrowdInvalidAuthenticationException(Exception innerException)
            : base(innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of an <code>CrowdInvalidAuthenticationException</code> with a default detail message using the
        /// name of the entity that failed to authenticate.
        /// </summary>
        /// <param name="name">name of entity</param>
        /// <returns>a new instance of <code>CrowdInvalidAuthenticationException</code></returns>
        public static CrowdInvalidAuthenticationException NewInstanceWithName(string name)
        {
            return new CrowdInvalidAuthenticationException("Account with name <" + name + "> failed to authenticate");
        }

        /// <summary>
        /// Creates a new instance of an <code>CrowdInvalidAuthenticationException</code> with a default detail message using the
        /// name of the entity that failed to authenticate, and a cause.
        /// </summary>
        /// <param name="name">name of entity</param>
        /// <param name="innerException">the cause</param>
        /// <returns>new instance of <code>CrowdInvalidAuthenticationException</code></returns>
        public static CrowdInvalidAuthenticationException NewInstanceWithName(string name, Exception innerException)
        {
            return new CrowdInvalidAuthenticationException("Account with name <" + name + "> failed to authenticate", innerException);
        }

        /// <summary>
        /// News the instance with name and description from cause.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <returns></returns>
        public static CrowdInvalidAuthenticationException NewInstanceWithNameAndDescriptionFromCause(string name, Exception innerException)
        {
            return new CrowdInvalidAuthenticationException("Account with name <" + name + "> failed to authenticate: "
                    + innerException.Message);
        }
    }
}