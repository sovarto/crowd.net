using Crowd.Rest.Client.Entity;
using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown to indicate an invalid model group.
    /// </summary>
    public class CrowdInvalidGroupException : CrowdException
    {
        public Group Group { get; private set; }

        /// <summary>
        /// Constructs a new <code>CrowdInvalidGroupException</code> with the invalid group given and a cause.
        /// </summary>
        /// <param name="group">the invalid group</param>
        /// <param name="innerException">the inner exception (a null value is permitted)</param>
        public CrowdInvalidGroupException(Group group, Exception innerException)
            : base(innerException)
        {
            this.Group = group;
        }

        /// <summary>
        /// Constructs a new <code>CrowdInvalidGroupException</code> with the invalid group and
        /// </summary>
        /// <param name="group">invalid group</param>
        /// <param name="message">detail message</param>
        public CrowdInvalidGroupException(Group group, string message)
            : base(message)
        {
            this.Group = group;
        }

        public CrowdInvalidGroupException(Group group, string message, Exception innerException)
            : base(message, innerException)
        {
            this.Group = group;
        }

    }
}