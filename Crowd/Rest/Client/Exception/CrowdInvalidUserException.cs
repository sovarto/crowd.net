using Crowd.Rest.Client.Entity;
using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// 
    /// </summary>
    public class CrowdInvalidUserException : CrowdException
    {
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public User User { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidUserException"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="message">The message.</param>
        public CrowdInvalidUserException(User user, string message)
            : base(message)
        {
            this.User = user;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidUserException"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdInvalidUserException(User user, Exception innerException)
            : base(innerException)
        {
            this.User = user;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidUserException"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdInvalidUserException(User user, string message, Exception innerException)
            : base(message, innerException)
        {
            this.User = user;
        }

    }
}