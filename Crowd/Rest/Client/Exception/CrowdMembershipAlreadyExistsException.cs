using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown to indicate that a membership cannot be added because it already exists.
    /// </summary>
    public class CrowdMembershipAlreadyExistsException : CrowdObjectAlreadyExistsException
    {
        /// <summary>
        /// Gets the child entity.
        /// </summary>
        /// <value>
        /// The child entity.
        /// </value>
        public string ChildEntity { get; private set; }
        /// <summary>
        /// Gets the parent entity.
        /// </summary>
        /// <value>
        /// The parent entity.
        /// </value>
        public string ParentEntity { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdMembershipAlreadyExistsException"/> class.
        /// </summary>
        /// <param name="directoryId">The directory identifier.</param>
        /// <param name="childEntity">The child entity.</param>
        /// <param name="parentEntity">The parent entity.</param>
        public CrowdMembershipAlreadyExistsException(long directoryId, string childEntity, string parentEntity)
            : base("Membership already exists in directory [" + directoryId + "] from child entity [" + childEntity
                  + "] to parent entity [" + parentEntity + "]")
        {
            this.ChildEntity = childEntity;
            this.ParentEntity = parentEntity;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdMembershipAlreadyExistsException"/> class.
        /// </summary>
        /// <param name="childEntity">The child entity.</param>
        /// <param name="parentEntity">The parent entity.</param>
        public CrowdMembershipAlreadyExistsException(string childEntity, string parentEntity)
            : base("Membership already exists from child entity [" + childEntity + "] to parent entity ["
                  + parentEntity + "]")
        {
            this.ChildEntity = childEntity;
            this.ParentEntity = parentEntity;
        }

    }
}