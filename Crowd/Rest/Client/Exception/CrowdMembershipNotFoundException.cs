using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Used to denote that a particular USER-GROUP or GROUP-GROUP membership
    /// does not exist.
    /// This could be thrown in cases where the calling code attempts to remove
    /// a user from a group when the user is not a direct member of the group, etc.
    /// </summary>
    public class CrowdMembershipNotFoundException : CrowdObjectNotFoundException
    {
        /// <summary>
        /// Gets the name of the child.
        /// </summary>
        /// <value>
        /// The name of the child.
        /// </value>
        public string ChildName { get; private set; }
        /// <summary>
        /// Gets the name of the parent.
        /// </summary>
        /// <value>
        /// The name of the parent.
        /// </value>
        public string ParentName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdMembershipNotFoundException"/> class.
        /// </summary>
        /// <param name="childName">Name of the child.</param>
        /// <param name="parentName">Name of the parent.</param>
        public CrowdMembershipNotFoundException(string childName, string parentName)
            : this(childName, parentName, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdMembershipNotFoundException"/> class.
        /// </summary>
        /// <param name="childName">Name of the child.</param>
        /// <param name="parentName">Name of the parent.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdMembershipNotFoundException(string childName, string parentName, Exception innerException)
            : base("The child entity <" + childName + "> is not a member of the parent <" + parentName + ">", innerException)
        {
            this.ChildName = childName;
            this.ParentName = parentName;
        }


    }

}