using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when a user attempts to add a group to another group in a Directory that does not support nested groups.
    /// </summary>
    public class CrowdNestedGroupsNotSupportedException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdNestedGroupsNotSupportedException"/> class.
        /// </summary>
        /// <param name="directoryId">The directory identifier.</param>
        public CrowdNestedGroupsNotSupportedException(long directoryId)
            : base("Directory with id <" + directoryId + "> does not support nested groups")
        {
        }
    }
}