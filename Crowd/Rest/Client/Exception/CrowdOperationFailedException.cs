using System;

namespace Crowd.Rest.Client
{
    /// <summary>
    /// Represents an error when executing an operation on the remote directory 
    /// failed for some reason. E.g. network error, LDAP errors.
    /// </summary>
    public class CrowdOperationFailedException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdOperationFailedException"/> class.
        /// </summary>
        public CrowdOperationFailedException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdOperationFailedException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdOperationFailedException(Exception innerException)
            : base(innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdOperationFailedException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CrowdOperationFailedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdOperationFailedException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public CrowdOperationFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}