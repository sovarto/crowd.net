﻿using System;
using System.Runtime.Serialization;
using Crowd.Rest.Client.Entity;
using System.Net;

namespace Crowd.Rest.Client
{
    /// <summary>
    /// Crowd Rest API exception class
    /// </summary>
    public class CrowdRestException : Exception
    {
        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public Error Error { get; private set; }
        /// <summary>
        /// Gets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public CrowdRestException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        /// <param name="error">The error.</param>
        /// <param name="statusCode">The status code.</param>
        public CrowdRestException(string message, Exception inner, Error error, HttpStatusCode statusCode)
            : base(message, inner)
        {
            // The Crowd API specific error information
            this.Error = error;
            this.StatusCode = statusCode;
        }

    }


    ///// <summary>
    ///// Crowd Rest API Bad Request (400) exception class
    ///// </summary>
    //public class CrowdRestBadRequestException : CrowdRestException
    //{
    //    public CrowdRestBadRequestException(string message, System.Exception inner, Error errorResponse, HttpStatusCode statusCode) : base(message, inner, errorResponse, statusCode) {}
    //}

    ///// <summary>
    ///// Crowd Rest API Unauthorized (401) exception class
    ///// </summary>
    //public class CrowdRestUnauthorizedException : CrowdRestException
    //{
    //    public CrowdRestUnauthorizedException(string message, System.Exception inner, Error errorResponse, HttpStatusCode statusCode) : base(message, inner, errorResponse, statusCode) { }
    //}

    ///// <summary>
    ///// Crowd Rest API Forbidden (403) exception class
    ///// </summary>
    //public class CrowdRestForbiddenException : CrowdRestException
    //{
    //    public CrowdRestForbiddenException(string message, System.Exception inner, Error errorResponse, HttpStatusCode statusCode) : base(message, inner, errorResponse, statusCode) { }
    //}

    ///// <summary>
    ///// Crowd Rest API Not Found (404) exception class
    ///// </summary>
    //public class CrowdRestNotFoundException : CrowdRestException
    //{
    //    public CrowdRestNotFoundException(string message, System.Exception inner, Error errorResponse, HttpStatusCode statusCode) : base(message, inner, errorResponse, statusCode) { }
    //}
    ///// <summary>
    ///// Crowd Rest API Conflict (409) exception class
    ///// </summary>
    //public class CrowdRestConflictException : CrowdRestException
    //{
    //    public CrowdRestConflictException(string message, System.Exception inner, Error errorResponse, HttpStatusCode statusCode) : base(message, inner, errorResponse, statusCode) { }
    //}
}
