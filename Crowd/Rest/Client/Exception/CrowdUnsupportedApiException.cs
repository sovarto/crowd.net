using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Failure because this API call is only supported by a later version of Crowd.
    /// </summary>
    public class CrowdUnsupportedApiException : CrowdOperationFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdUnsupportedApiException"/> class.
        /// </summary>
        /// <param name="requiredVersion">The required version.</param>
        /// <param name="functionality">The functionality.</param>
        public CrowdUnsupportedApiException(string requiredVersion, string functionality)
            : base("Crowd REST API version " + requiredVersion
                    + " or greater is required on the server " + functionality + ".")
        {
        }
    }
}