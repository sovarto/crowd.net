using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the specified user could not be found.
    /// </summary>
    public class CrowdUserNotFoundException : CrowdObjectNotFoundException
    {
        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdUserNotFoundException"/> class.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        public CrowdUserNotFoundException(string userName)
            : this(userName, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdUserNotFoundException"/> class.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdUserNotFoundException(string userName, Exception innerException)
            : base(string.Format("User <{0}> does not exist", userName), innerException)
        {
            this.UserName = userName;
        }

    }
}