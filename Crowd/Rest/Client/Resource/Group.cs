﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Group

        /// <summary>
        /// Get a group by name.
        /// </summary>
        /// <param name="groupName">Name of the group to retrieve.</param>
        /// <returns>A group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">The group does not exist on the remote server.</exception>
        /// <exception cref="CrowdOperationFailedException">For any other communication errors.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        public Group GetGroup(string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "group.json?groupname={0}".Fmt(groupName.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<Group>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

        }

        /// <summary>
        /// Get a group with attributes by name.
        /// </summary>
        /// <param name="groupName">Name of the group to retrieve.</param>
        /// <returns>A Group with attributes.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group does not exist on the remote server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public GroupWithAttributes GetGroupWithAttributes(string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "group.json?groupname={0}&expand=attributes".Fmt(groupName.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<GroupWithAttributes>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Adds a group to the remote Crowd server.
        /// </summary>
        /// <param name="newGroup">The group to add.</param>
        /// <exception cref="CrowdInvalidGroupException">if the group is invalid. This may be because a group of the same name already exists, or does not pass the server side validation rules.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void AddGroup(Group newGroup)
        {
            try
            {
                // Our api resource
                string apiResource = "group.json";
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = newGroup;

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidGroup(e.Error, newGroup);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Updates a group on the remote Crowd server.
        /// </summary>
        /// <param name="existingGroup">The existing group to update.</param>
        /// <returns>The updated group.</returns>
        /// <exception cref="CrowdInvalidGroupException">The group is invalid. This may be because the group does not pass the server side validation rules.</exception>
        /// <exception cref="CrowdGroupNotFoundException">if the group does not exist on the remote server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public Group UpdateGroup(Group existingGroup)
        {
            if (existingGroup == null)
                throw new ArgumentNullException();
            if (existingGroup.Name == null)
                throw new ArgumentNullException();

            try
            {
                // Our api resource
                string apiResource = "group.json?groupname={0}".Fmt(existingGroup.Name.UrlEncode());
                string httpMethod = "PUT";

                //  Create our arguments object:
                object args = existingGroup;

                //  Make the call:
                return MakeAPICall<Group>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidGroup(e.Error, existingGroup);
                HandleGroupNotFound(e.Error, existingGroup.Name);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Stores the group's attributes on the remote Crowd server.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="attributes">Set of Attributes to store. Attributes will be added or
        /// if an attribute with the same key exists will be replaced.</param>
        /// <exception cref="CrowdGroupNotFoundException">if the group does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void StoreGroupAttributes(string groupName, AttributeList attributes)
        {
            try
            {
                // Our api resource
                string apiResource = "group/attribute.json?groupname={0}".Fmt(groupName.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = attributes;

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);

            }
        }

        /// <summary>
        /// Gets the group's attributes from the remote Crowd server.
        /// </summary>
        /// <param name="groupName">The name for the group.</param>
        /// <returns>
        /// A list of attributes for the group.
        /// </returns>
        public List<Entity.Attribute> GetGroupAttributes(string groupName)
        {
            AttributeList attributes;
            try
            {
                // Our api resource
                string apiResource = "group/attribute.json?groupname={0}".Fmt(groupName.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                attributes = MakeAPICall<AttributeList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                // TODO: GetGroupAttributes - Check exceptions thrown
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return attributes.ToAttributeList();
        }

        /// <summary>
        /// Removes a group attribute (set) from the server. 
        /// If the attribute to be removed does not exist, no error is reported.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="attributeName">The key of the attribute to remove.</param>
        /// <exception cref="CrowdGroupNotFoundException">the group does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RemoveGroupAttributes(string groupName, string attributeName)
        {
            try
            {
                // Our api resource
                string apiResource = "group/attribute.json?groupname={0}&attributename={1}".Fmt(
                                                    groupName.UrlEncode()
                                                    , attributeName.UrlEncode()
                                                   );
                string httpMethod = "DELETE";

                //  Make the call:
                MakeAPICall<Group>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Removes a group from the remote Crowd server.
        /// </summary>
        /// <param name="groupName">The name of the group to remove.</param>
        /// <exception cref="CrowdGroupNotFoundException">the group does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RemoveGroup(string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "group.json?groupname={0}".Fmt(groupName.UrlEncode());
                string httpMethod = "DELETE";

                //  Make the call:
                MakeAPICall<Group>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Determines whether the child group is a direct member of the parent group.
        /// </summary>
        /// <param name="childName">The name of the child group.</param>
        /// <param name="parentName">The name of the parent group.</param>
        /// <returns>true if the child group is a direct member of the parent group.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public bool IsGroupDirectGroupMember(string childName, string parentName)
        {
            try
            {
                // Our api resource
                string apiResource = "group/child-group/direct?groupname={0}&child-groupname={1}".Fmt(parentName.UrlEncode(), childName.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                Group response = MakeAPICall<Group>(apiResource, httpMethod);

                return response.Name.Equals(childName, StringComparison.CurrentCultureIgnoreCase);
            }
            catch (CrowdRestException e)
            {
                // Catch User/Group Not Found Error
                if (e.StatusCode == HttpStatusCode.NotFound)
                    return false;

                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Adds a group to a group.
        /// </summary>
        /// <param name="childGroup">The group to add to the parent group.</param>
        /// <param name="parentGroup">The name of the group to which the child will be added to</param>
        public void AddGroupToGroup(string childGroup, string parentGroup)
        {
            try
            {
                // Our api resource
                string apiResource = "group/child-group/direct?groupname={0}".Fmt(parentGroup.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = new Group { Name = childGroup };

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);

            }
            catch (CrowdRestException e)
            {
                HandleMembershipAlreadyExists(e.Error, childGroup, parentGroup);
                switch (e.StatusCode)
                {
                    case HttpStatusCode.NotFound:
                        HandleGroupNotFound(e.Error, parentGroup);
                        break;
                    case HttpStatusCode.BadRequest:
                        HandleGroupNotFound(e.Error, childGroup);
                        break;
                }
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Removes a group from a group.
        /// </summary>
        /// <param name="childGroup">The name of the group to be removed from the parent group.</param>
        /// <param name="parentGroup">The name of the group the child group will be removed from.</param>
        /// <exception cref="CrowdMembershipNotFoundException">if there is not parent-child relationship between the specified groups.</exception>
        /// <exception cref="CrowdGroupNotFoundException">if either group does not exist.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if for some reason the operation has failed.</exception>
        public void RemoveGroupFromGroup(string childGroup, string parentGroup)
        {
            try
            {
                // Our api resource
                string apiResource = "group/child-group/direct?groupname={0}&child-groupname={1}".Fmt(
                                                                        parentGroup.UrlEncode()
                                                                        , childGroup.UrlEncode()
                                                                        );
                string httpMethod = "DELETE";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleMembershipNotFound(e.Error, childGroup, parentGroup);
                GetGroup(childGroup);
                GetGroup(parentGroup);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Gets the users who are direct members of a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of users who are direct members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<User> GetUsersOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            UserList users;
            try
            {
                // Our api resource
                string apiResource = "group/user/direct.json?groupname={0}&start-index={1}&max-results={2}&expand=user".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                users = MakeAPICall<UserList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return users.ToUserList();
        }

        /// <summary>
        /// Gets the names of users who are direct members of a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of user names for users who are direct members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfUsersOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            UserList users;
            try
            {
                // Our api resource
                string apiResource = "group/user/direct.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                users = MakeAPICall<UserList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return users.ToNameList();
        }

        /// <summary>
        /// Gets the groups who are direct members of a specified group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of groups who are direct members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetChildGroupsOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList childGroups;
            try
            {
                // Our api resource
                string apiResource = "group/child-group/direct?groupname={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                childGroups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return childGroups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of groups who are direct members of a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of group names for groups who are direct members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfChildGroupsOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "group/child-group/direct.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return groups.ToNameList();
        }

        /// <summary>
        /// Gets the groups a specified group is a direct member of.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of groups the group is a direct member of.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetParentGroupsForGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList parentGroups;
            try
            {
                // Our api resource
                string apiResource = "group/parent-group/direct.json?groupname={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                parentGroups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return parentGroups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of the groups a specified group is a direct member of.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of group names for the groups the group is a direct member of.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfParentGroupsForGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList parentGroups;
            try
            {
                // Our api resource
                string apiResource = "group/parent-group/direct.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                parentGroups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return parentGroups.ToNameList();
        }

        /// <summary>
        /// Gets the users who are nested members of a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of users who are nested members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<User> GetNestedUsersOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            UserList users;
            try
            {
                // Our api resource
                string apiResource = "group/user/nested.json?groupname={0}&start-index={1}&max-results={2}&expand=user".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                users = MakeAPICall<UserList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return users.ToUserList();
        }

        /// <summary>
        /// Gets the names of users who are nested members of a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of names for the users who are nested members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfNestedUsersOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            UserList users;
            try
            {
                // Our api resource
                string apiResource = "group/user/nested.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                users = MakeAPICall<UserList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return users.ToNameList();
        }

        /// <summary>
        /// Gets the groups that are nested members of the specified group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of groups that are nested members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetNestedChildGroupsOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "group/child-group/nested.json?groupname={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return groups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of the groups that are nested members of the specified group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of names for the groups that are nested members of a group.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesofNestedChildGroupsOfGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "group/child-group/nested.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return groups.ToNameList();
        }

        /// <summary>
        /// Gets the groups that a specified group is a nested member of.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="startIndex">The start index.</param>
        /// <param name="maxResults">The maximum results.</param>
        /// <returns>A list of groups that a group is a nested member of.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetParentGroupsForNestedGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList parentGroups;
            try
            {
                // Our api resource
                string apiResource = "group/parent-group/nested.json?groupname={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                parentGroups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return parentGroups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of the groups that a specified group is a nested member of.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="startIndex">The start index.</param>
        /// <param name="maxResults">The maximum results.</param>
        /// <returns>A list of names of the groups that a group is a nested member of.</returns>
        /// <exception cref="CrowdGroupNotFoundException">if the group could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfParentGroupsForNestedGroup(string groupName, int startIndex = 0, int maxResults = -1)
        {
            GroupList parentGroups;
            try
            {
                // Our api resource
                string apiResource = "group/parent-group/nested.json?groupname={0}&start-index={1}&max-results={2}".Fmt(
                                                groupName.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                parentGroups = MakeAPICall<GroupList>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            return parentGroups.ToNameList();
        }

        /// <summary>
        /// Gets the full group membership details for all groups with all direct user members and child groups.
        /// </summary>
        /// <returns>A list of the memberships for all groups.</returns>
        /// <remarks>
        /// The result may be large and this operation may be slow.
        /// This method is only supported when the server supports version 1.1 of the user management API. 
        /// Clients should be ready to catch UnsupportedCrowdApiException and fall back to another technique
        /// if they need to remain backwards compatible.
        /// </remarks>
        /// <exception cref="CrowdUnsupportedApiException">if the server does not support version 1.1 of the user management API.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        public List<GroupMembership> GetMemberships()
        {
            // TODO: Implement GetMemberships API call
            throw new NotImplementedException();
        }

        #endregion

    }
}

