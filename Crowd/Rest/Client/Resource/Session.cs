﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Session

        /// <summary>
        /// Finds the user from the specified user token.
        /// </summary>
        /// <param name="token">The user token used to find the authenticated user.</param>
        /// <returns>User associated with the token.</returns>
        /// <exception cref="CrowdInvalidTokenException">if the provided token is not valid.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public User FindUserFromSSOToken(string token)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json?expand=user".Fmt(token.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod).User;
            }
            catch (CrowdRestException e)
            {
                HandleInvalidSsoToken(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Authenticates a Single-Sign-On (SSO) User.
        /// </summary>
        /// <param name="authenticationContext">The user's authentication details.</param>
        /// <returns>The SSO token if successful.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CrowdApplicationAccessDeniedException">if the user does not have access to authenticate against the application.</exception>
        /// <exception cref="CrowdExpiredCredentialException">if the user password has expired and the user is required to change their password.</exception>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public string AuthenticateSSOUser(AuthenticationContext authenticationContext)
        {
            if (authenticationContext == null)
                throw new ArgumentNullException();
            if (authenticationContext.UserName == null)
                throw new ArgumentNullException();

            try
            {
                // Our api resource
                string apiResource = "session?validate-password=true";
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = authenticationContext;

                //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod, args).Token;
            }
            catch (CrowdRestException e)
            {
                Error error = e.Error;
                HandleInvalidUserAuthentication(error, authenticationContext.UserName);
                HandleInactiveUserAccount(error, authenticationContext.UserName);
                HandleExpiredUserCredential(error);
                HandleApplicationAccessDenied(error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Authenticates a Single-Sign-On (SSO) User.
        /// </summary>
        /// <param name="authenticationContext">The user's authentication details.</param>
        /// <param name="duration">The requested duration of the new token (in seconds).</param>
        /// <returns>
        /// The SSO token if successful.
        /// </returns>
        /// <exception cref="CrowdApplicationAccessDeniedException">if the user does not have access to authenticate against the application.</exception>
        /// <exception cref="CrowdExpiredCredentialException">if the user password has expired and the user is required to change their password.</exception>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public string AuthenticateSSOUser(AuthenticationContext authenticationContext, long duration)
        {
            if (authenticationContext == null)
                throw new ArgumentNullException();
            if (authenticationContext.UserName == null)
                throw new ArgumentNullException();

            try
            {
                // Our api resource
                string apiResource = "session?validate-password=true&duration={0}".Fmt(duration.ToString());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = authenticationContext;

                //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod, args).Token;
            }
            catch (CrowdRestException e)
            {
                Error error = e.Error;
                HandleInvalidUserAuthentication(error, authenticationContext.UserName);
                HandleInactiveUserAccount(error, authenticationContext.UserName);
                HandleExpiredUserCredential(error);
                HandleApplicationAccessDenied(error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Authenticates a Single-Sign-On (SSO) User without validating password.
        /// </summary>
        /// <param name="authenticationContext">The user's authentication details.</param>
        /// <returns>The SSO token if successful.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CrowdApplicationAccessDeniedException">if the user does not have access to authenticate against the application.</exception>
        /// <exception cref="CrowdExpiredCredentialException">if the user password has expired and the user is required to change their password.</exception>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public string AuthenticateSSOUserWithoutValidatingPassword(AuthenticationContext authenticationContext)
        {
            if (authenticationContext == null)
                throw new ArgumentNullException();
            if (authenticationContext.UserName == null)
                throw new ArgumentNullException();

            try
            {
                // Our api resource
                string apiResource = "session?validate-password=false";
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = authenticationContext;

                //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod, args).Token;
            }
            catch (CrowdRestException e)
            {
                Error error = e.Error;
                HandleInvalidUserAuthentication(error, authenticationContext.UserName);
                HandleInactiveUserAccount(error, authenticationContext.UserName);
                HandleApplicationAccessDenied(error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Validates the SSO authentication. Throws InvalidAuthenticationException if the SSO authentication is not valid.
        /// </summary>
        /// <param name="token">Crowd SSO token.</param>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void ValidateSSOAuthentication(string token)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json".Fmt(token.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                MakeAPICall<SSOSession>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidSsoToken(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Validates the SSO authentication. Throws InvalidAuthenticationException if the SSO authentication is not valid.
        /// </summary>
        /// <param name="token">Crowd SSO token.</param>
        /// <param name="validationFactors">Details of where the user's come from. If presented, must match those presented during authentication.</param>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void ValidateSSOAuthentication(string token, ValidationFactors validationFactors)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json".Fmt(token.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = validationFactors;

                //  Make the call:
                MakeAPICall<SSOSession>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidSsoToken(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Validates the SSO authentication. Throws InvalidAuthenticationException if the SSO authentication is not valid.
        /// </summary>
        /// <param name="token">Crowd SSO token.</param>
        /// <returns>The current session.</returns>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public SSOSession ValidateSSOAuthenticationAndGetSession(string token)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json".Fmt(token.UrlEncode());
                string httpMethod = "GET";

                 //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidSsoToken(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Validates the SSO authentication. Throws InvalidAuthenticationException if the SSO authentication is not valid.
        /// </summary>
        /// <param name="token">Crowd SSO token.</param>
        /// <param name="validationFactors">Details of where the user's come from. If presented, must match those presented during authentication.</param>
        /// <returns>The current session.</returns>
        /// <exception cref="CrowdInactiveAccountException">if the user account is inactive.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public SSOSession ValidateSSOAuthenticationAndGetSession(string token, ValidationFactors validationFactors)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json".Fmt(token.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = validationFactors;

                //  Make the call:
                return MakeAPICall<SSOSession>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidSsoToken(e.Error);
                throw HandleCommonExceptions(e);
            }
        }
        
        /// <summary>
        /// Invalidates the sesson token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void InvalidateSSOToken(string token)
        {
            try
            {
                // Our api resource
                string apiResource = "session/{0}.json".Fmt(token.UrlEncode());
                string httpMethod = "DELETE";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Invalidates all tokens for a given user name.
        /// </summary>
        /// <param name="username">The name of the user whose sessions will be invalidated.</param>
        /// <param name="exclude">The token to be excluded from invalidation (i.e. saved).</param>
        /// <exception cref="CrowdUnsupportedApiException">if the remote server does not support this operation.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void InvalidateSSOTokensForUser(string username, string exclude = null)
        {
            try
            {
                // Our api resource
                string apiResource = "session.json?username={0}".Fmt(username.UrlEncode());
                if (!string.IsNullOrWhiteSpace(exclude)) apiResource += "&exclude={0}".Fmt(exclude.UrlEncode());
                string httpMethod = "DELETE";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                if (e.StatusCode == HttpStatusCode.MethodNotAllowed)
                {
                    throw new CrowdUnsupportedApiException("1.3", "for bulk session invalidation");
                }
                else
                {
                    throw HandleCommonExceptions(e);
                }
            }
        }

        #endregion

    }
}

