﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region Fields and properties
        private const string USER_NULL_ERROR_MSG = "User must not be null";
        private const string USERNAME_NULL_ERROR_MSG = "Username must not be null";
        #endregion

        #region API : User

        /// <summary>
        /// Gets a user by username.
        /// </summary>
        /// <param name="username">Name of the user to retrieve.</param>
        /// <returns>
        /// A User
        /// </returns>
        /// <exception cref="CrowdUserNotFoundException">if the user is not found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public User GetUser(string username)
        {
            try
            {
                // Our api resource
                string apiResource = "user.json?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<User>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Gets a user with attributes by username.
        /// </summary>
        /// <param name="username">Name of the user to retrieve.</param>
        /// <returns>
        /// A User
        /// </returns>
        /// <exception cref="CrowdUserNotFoundException">if the user is not found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public UserWithAttributes GetUserWithAttributes(string username)
        {
            try
            {
                // Our api resource
                string apiResource = "user.json?username={0}&expand=attributes".Fmt(username.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<UserWithAttributes>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Adds a new User to the remote Crowd server.
        /// </summary>
        /// <param name="newUser">The user to add</param>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidUserException">if the user is invalid. This may be because a user of the same name already exists, or does not pass the server side validation rules.</exception>
        /// <exception cref="CrowdInvalidCredentialException">if the password is invalid. It must conform to the rules set on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void AddUser(User newUser)
        {
            if (newUser == null)
                throw new ArgumentNullException(USER_NULL_ERROR_MSG);
            if (newUser.Name == null)
                throw new ArgumentNullException(USERNAME_NULL_ERROR_MSG);

            try
            {
                // Our api resource
                string apiResource = "user.json";
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = newUser;

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidUser(e.Error, newUser);
                HandleInvalidCredential(e.Error);
                throw HandleCommonExceptions(e);
            }

        }

        /// <summary>
        /// Updates a user on the remote Crowd server.
        /// </summary>
        /// <param name="existingUser">The existing user to update</param>
        /// <exception cref="CrowdInvalidUserException">the details of the user to be updated are invalid. This may be because the user details do not pass the server side validation rules.</exception>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void UpdateUser(User existingUser)
        {
            if (existingUser == null)
                throw new ArgumentNullException(USER_NULL_ERROR_MSG);
            if (existingUser.Name == null)
                throw new ArgumentNullException(USERNAME_NULL_ERROR_MSG);

            try
            {
                // Our api resource
                string apiResource = "user.json?username={0}".Fmt(existingUser.Name.UrlEncode());
                string httpMethod = "PUT";

                //  Create our arguments object:
                object args = existingUser;

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidUser(e.Error, existingUser);
                HandleUserNotFound(e.Error, existingUser.Name);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Updates the user's password on the remote Crowd server.
        /// </summary>
        /// <param name="username">The username of the user to update.</param>
        /// <param name="password">The new password.</param>
        /// <exception cref="CrowdInvalidCredentialException">if the password is invalid. It must conform to the rules set on the server.</exception>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void UpdateUserPassword(string username, string password)
        {
            try
            {
                // Our api resource
                string apiResource = "user/password?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "PUT";

                //  Create our arguments object:
                object args = new Password { value = password };

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                HandleInvalidCredential(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Requests the password reset.
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist.</exception>
        /// <exception cref="CrowdInvalidEmailAddressException">if the user does not have a valid email to send the reset password link to.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RequestPasswordReset(string username)
        {
            try
            {
                // Our api resource
                string apiResource = "user/mail/password?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "POST";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                HandleInvalidEmail(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Sends an email reminder of the usernames associated with the given email address.
        /// No email will be sent if there are no usernames associated with a given email.
        /// </summary>
        /// <param name="email">The email address of the user.</param>
        /// <exception cref="CrowdInvalidEmailAddressException">if the email is not valid.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RequestUsernames(string email)
        {
            try
            {
                // Our api resource
                string apiResource = "user/mail/usernames?email={0}".Fmt(email.UrlEncode());
                string httpMethod = "POST";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleInvalidEmail(e.Error);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Stores the user's attributes on the remote Crowd server.
        /// </summary>
        /// <param name="username">The username for the user.</param>
        /// <param name="attributes">The set of Attributes to store. Attributes will be added.</param>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void StoreUserAttributes(string username, AttributeList attributes)
        {
            try
            {
                // Our api resource
                string apiResource = "user/attribute.json?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = attributes;

                //  Make the call:
                MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Gets the user's attributes from the remote Crowd server.
        /// </summary>
        /// <param name="username">The username for the user.</param>
        /// <returns>A list of attributes for the user</returns>
        public List<Entity.Attribute> GetUserAttributes(string username)
        {
            AttributeList attributes;
            try
            {
                // Our api resource
                string apiResource = "user/attribute.json?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "GET";

                //  Make the call:
                attributes = MakeAPICall<AttributeList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                // TODO: GetUserAttributes - Check exceptions thrown
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }

            return attributes.ToAttributeList();
        }

        /// <summary>
        /// Removes a user attribute from the server. If the attribute to be removed does not exist, no error is reported.
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <param name="attributeName">The key of the attribute.</param>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RemoveUserAttributes(string username, string attributeName)
        {
            try
            {
                // Our api resource
                string apiResource = "user/attribute.json?username={0}&attributename={1}".Fmt(username.UrlEncode(), attributeName.UrlEncode());
                string httpMethod = "DELETE";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Removes a user from the remote Crowd server.
        /// </summary>
        /// <param name="username">The username of the user to remove.</param>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist on the remote Crowd server.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RemoveUser(string username)
        {
            try
            {
                // Our api resource
                string apiResource = "user.json?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "DELETE";

                //  Make the call:
                MakeAPICall<Group>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Determines whether a user is a direct member of a group.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="groupName">The name of the group.</param>
        /// <returns>true if the user is a direct member of the group.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public bool IsUserDirectGroupMember(string username, string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "user/group/direct.json?username={0}&groupname={1}".Fmt(
                                                username.UrlEncode()
                                                , groupName.UrlEncode()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                Group response = MakeAPICall<Group>(apiResource, httpMethod);

                return response.Name.Equals(groupName, StringComparison.CurrentCultureIgnoreCase);
            }
            catch (CrowdRestException e)
            {
                // Catch User/Group Not Found Error
                if (e.StatusCode == HttpStatusCode.NotFound)
                    return false;

                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Determines whether a user is a nested member of a group.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="groupName">The name of the group.</param>
        /// <returns>true if the user is a nested member of the group.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public bool IsUserNestedGroupMember(string username, string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "user/group/nested.json?username={0}&groupname={1}".Fmt(
                                                username.UrlEncode()
                                                , groupName.UrlEncode()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                Group response = MakeAPICall<Group>(apiResource, httpMethod);

                return response.Name.Equals(groupName, StringComparison.CurrentCultureIgnoreCase);
            }
            catch (CrowdRestException e)
            {
                // Catch User/Group Not Found Error
                if (e.StatusCode == HttpStatusCode.NotFound)
                    return false;

                throw HandleCommonExceptions(e);
            }

        }

        /// <summary>
        /// Adds a user to a group.
        /// </summary>
        /// <param name="username">The username of the user to add to the group.</param>
        /// <param name="groupName">The name of the group to be added to.</param>
        /// <exception cref="CrowdGroupNotFoundException">if the group does not exist.</exception>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        /// <exception cref="CrowdMembershipAlreadyExistsException">if the user is already a member fo the group.</exception>
        public void AddUserToGroup(string username, string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "user/group/direct?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = new Group { Name = groupName };

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod, args);
            }
            catch (CrowdRestException e)
            {
                HandleMembershipAlreadyExists(e.Error, username, groupName);
                HandleUserNotFound(e.Error, username);
                HandleGroupNotFound(e.Error, groupName);
                throw HandleCommonExceptions(e);
            }

            }

        /// <summary>
        /// Removes a user from a group.
        /// </summary>
        /// <param name="username">The username of the user to add to the group.</param>
        /// <param name="groupName">The name of the group to be added to.</param>
        /// <exception cref="CrowdMembershipNotFoundException">if the membership does not exist.</exception>
        /// <exception cref="CrowdGroupNotFoundException">if the group does not exist.</exception>
        /// <exception cref="CrowdUserNotFoundException">if the user does not exist.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public void RemoveUserFromGroup(string username, string groupName)
        {
            try
            {
                // Our api resource
                string apiResource = "user/group/direct?username={0}&groupname={1}".Fmt(
                                                                        username.UrlEncode()
                                                                        , groupName.UrlEncode()
                                                                        );
                string httpMethod = "DELETE";

                //  Make the call:
                var response = MakeAPICall<object>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleMembershipNotFound(e.Error, username, groupName);
                HandleGroupNotFound(e.Error, groupName);
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Gets the groups that a user is a direct member of.
        /// </summary>
        /// <param name="username">The username for a user.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of groups that the user is a direct member of.</returns>
        /// <exception cref="CrowdUserNotFoundException">if the user could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetGroupsForUser(string username, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "user/group/direct.json?username={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                username.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
            return groups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of the groups a user is a direct member of.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of group names the user is a direct member of.</returns>
        /// <exception cref="CrowdUserNotFoundException">if the user could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfGroupsForUser(string username, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "user/group/direct.json?username={0}&start-index={1}&max-results={2}".Fmt(
                                                username.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }
            return groups.ToNameList();
        }

        /// <summary>
        /// Gets the groups that a user is a nested member of.
        /// </summary>
        /// <param name="username">The username of a user.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of groups that a user is a nested member of.</returns>
        /// <exception cref="CrowdUserNotFoundException">if the user could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> GetGroupsForNestedUser(string username, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "user/group/nested.json?username={0}&start-index={1}&max-results={2}&expand=group".Fmt(
                                                username.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }

            return groups.ToGroupList();
        }

        /// <summary>
        /// Gets the names of groups that a user is a nested member of.
        /// </summary>
        /// <param name="username">The username of a user.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>A list of names of the groups that a user is a nested member of.</returns>
        /// <exception cref="CrowdUserNotFoundException">if the user could not be found.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> GetNamesOfGroupsForNestedUser(string username, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                string apiResource = "user/group/nested.json?username={0}&start-index={1}&max-results={2}".Fmt(
                                                username.UrlEncode()
                                                , startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                string httpMethod = "GET";

                //  Make the call:
                groups = MakeAPICall<GroupList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                throw HandleCommonExceptions(e);
            }

            return groups.ToNameList();
        }

        #endregion

        #region API : Test Connection

        /// <summary>
        /// Tests if the connection to the crowd server is OK. 
        /// </summary>
        /// <remarks>
        /// This test uses a user search to validate the connection. 
        /// It will fail if the application does not have permission to perform this very basic operation.
        /// </remarks>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the test fails.</exception>
        public void TestConnection()
        {
            // This test does a get Users query returning at most 1 user.
            // The test may return no users, but should never fail with an exception if the directory is correctly configured.
            SearchUsers(0, 1);
        }

        #endregion

    }
}

