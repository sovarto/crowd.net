﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Security;

using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Web.Security
{
    /// <summary>
    /// 
    /// </summary>
    public class CrowdMembershipProvider : MembershipProvider
    {
        #region MembershipProvider Fields and Properties
        // keeps track of whether the provider has already been initialized 
        private bool initialized = false;
        // configuration parameters common to all membership providers 
        private string appName;
        private bool enablePasswordReset;
        private bool enablePasswordRetrieval = false;
        private int maxInvalidPasswordAttempts;
        private int minRequiredNonalphanumericCharacters;
        private int minRequiredPasswordLength;
        private int passwordAttemptWindow;
        private string passwordStrengthRegularExpression;
        private bool requiresQuestionAndAnswer = false;
        private bool requiresUniqueEmail;
        
        public override string ApplicationName
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return appName;
            }
            set
            {
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "ApplicationName"));
            }
        }
        public override bool EnablePasswordReset
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return enablePasswordReset;
            }
        }
        public override bool EnablePasswordRetrieval
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return enablePasswordRetrieval;
            }
        }
        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return maxInvalidPasswordAttempts;
            }
        }
        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return minRequiredNonalphanumericCharacters;
            }
        }
        public override int MinRequiredPasswordLength
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return minRequiredPasswordLength;
            }
        }
        public override int PasswordAttemptWindow
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return passwordAttemptWindow;
            }
        }
        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                //
                // Crowd membership provider does not support password retrieval 
                // (regardless of the settings). As a result the provider operates as 
                // if the password was effectively hashed.
                // 
                return MembershipPasswordFormat.Hashed;
            }
        }
        public override string PasswordStrengthRegularExpression
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return passwordStrengthRegularExpression;
            }
        }
        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return requiresQuestionAndAnswer;
            }
        }
        public override bool RequiresUniqueEmail
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(this.exceptionMessageProviderNotInitialized);

                return requiresUniqueEmail;
            }
        }
        
        // crowd client
        private CrowdRestClientManager crowd;
        // password size for autogenerating password 
        private const int PASSWORD_SIZE = 14;

        // provider exception messages 
        private string exceptionMessageProviderNotInitialized = "The provider has not been initialized.";
        private string exceptionMessageUnexpected = "An unexpected error occurred whilst initializing the provider.";
        private string exceptionMessageSettingNotSupported = "The setting <{0}> is not supported by this provider.";
        private string exceptionMessageSettingMustNotBeEmpty = "The setting <{0}> must not be empty.";
        private string exceptionMessageSettingIsRequired = "The setting <{0}> is required.";
        private string exceptionMessageSettingUnrecognized = "The setting <{0}> is not recognized.";
        private string exceptionMessageArgumentInvalid = "The argument <{0}> is invalid.";
        private string exceptionMessageUserNotFound = "The user <{0}> was not found.";
        private string exceptionMessageUserLockedOut = "The user <{0}> is locked out.";
        private string exceptionMessageGroupNotFound = "The role <{0}> was not found.";
        #endregion

        #region ProviderBase Methods
        /// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="System.ArgumentNullException">config</exception>
        /// <exception cref="System.Configuration.Provider.ProviderException">
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">The attribute <MinRequiredNonalphanumericCharacters> can not be more than the attribute <MinRequiredPasswordLength>.</exception>
        public override void Initialize(string name, NameValueCollection config)
        {
            if (initialized)
                return; 

            // Initialize values from web.config.
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CrowdMembershipProvider";

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Membership Provider for Atlassian Crowd");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            try
            {
                // Store provider properties

                this.appName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

                // credentials 
                // baseurl, username and password if specified must not be empty, moreover if one is specified the others must
                // be specified as well
                string baseUrl = config["crowdBaseUrl"];
                if (baseUrl != null && baseUrl.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdBaseUrl"));

                string username = config["crowdApplicationUserName"];
                if (username != null && username.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdApplicationUserName"));

                string password = config["crowdApplicationPassword"];
                if (password != null && password.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdApplicationPassword"));

                if ((username != null && password == null) || (password != null && username == null))
                    throw new ProviderException(string.Format(exceptionMessageSettingIsRequired, "Crowd Application Credentials"));
                NetworkCredential credential = new NetworkCredential(username, password, baseUrl);

                // Standard provider settings
                requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
                enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
                minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
                minRequiredNonalphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonalphanumericCharacters"], "1"));

                passwordStrengthRegularExpression = config["passwordStrengthRegularExpression"];
                if (passwordStrengthRegularExpression != null)
                {
                    passwordStrengthRegularExpression = passwordStrengthRegularExpression.Trim();
                    if (passwordStrengthRegularExpression.Length != 0)
                    {
                        try
                        {
                            Regex regex = new Regex(passwordStrengthRegularExpression);
                        }
                        catch (ArgumentException e)
                        {
                            throw new ProviderException(e.Message, e);
                        }
                    }
                }
                else
                {
                    passwordStrengthRegularExpression = string.Empty;
                }
                if (minRequiredNonalphanumericCharacters > minRequiredPasswordLength)
                    throw new ArgumentOutOfRangeException("The attribute <MinRequiredNonalphanumericCharacters> can not be more than the attribute <MinRequiredPasswordLength>.");

                // Initialise Crowd Client
                crowd = new CrowdRestClientManager(credential.Domain, credential.UserName, credential.Password);

                // Check configuration
                config.Remove("name");
                config.Remove("applicationName");
                config.Remove("description");
                config.Remove("enablePasswordReset");
                config.Remove("maxInvalidPasswordAttempts");
                config.Remove("minRequiredNonalphanumericCharacters");
                config.Remove("minRequiredPasswordLength");
                config.Remove("passwordAttemptWindow");
                config.Remove("passwordStrengthRegularExpression");
                config.Remove("requiresUniqueEmail");

                config.Remove("crowdBaseUrl");
                config.Remove("crowdApplicationUserName");
                config.Remove("crowdApplicationPassword");

                if (config.Count > 0)
                {
                    string attribUnrecognized = config.GetKey(0);
                    if (!string.IsNullOrEmpty(attribUnrecognized))
                        throw new ProviderException(string.Format(exceptionMessageSettingUnrecognized, attribUnrecognized));
                }

                initialized = true;

            }
            catch (ProviderException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ProviderException(exceptionMessageProviderNotInitialized, ex);
            }

        }
        #endregion

        #region MembershipProvider Methods
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            ValidatePasswordEventArgs e = new ValidatePasswordEventArgs(username, newPassword, false);
            OnValidatingPassword(e);
            if (e.Cancel)
            {
                if (e.FailureInformation != null)
                    throw e.FailureInformation;
                else
                    throw new ArgumentException(string.Format("Custom_Password_Validation_Failure"), "newPassword");
            }

            try
            {
                crowd.UpdateUserPassword(username, newPassword);
            }
            catch (CrowdApplicationPermissionException)
            {
                // Application is not allowed to delete a new user!
                return false;
            }
            catch (CrowdUserNotFoundException)
            {
                // the user could not be found
                return false;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return true;

        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            status = (MembershipCreateStatus)0;
            MembershipUser user = null;

            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (providerUserKey != null)
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "providerUserKey"));

            if ((passwordQuestion != null))
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "passwordQuestion"));

            if ((passwordAnswer != null) )
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "passwordAnswer"));

            if (password.Length < MinRequiredPasswordLength)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            int count = 0;

            for (int i = 0; i < password.Length; i++)
            {
                if (!char.IsLetterOrDigit(password, i))
                {
                    count++;
                }
            }

            if (count < MinRequiredNonAlphanumericCharacters)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (PasswordStrengthRegularExpression.Length > 0)
            {
                if (!Regex.IsMatch(password, PasswordStrengthRegularExpression))
                {
                    status = MembershipCreateStatus.InvalidPassword;
                    return null;
                }
            }

            ValidatePasswordEventArgs e = new ValidatePasswordEventArgs(username, password, true);
            OnValidatingPassword(e);

            if (e.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            try
            {

                User newUser = new User { Name = username, Password = new Password { value = password }, Email = email, IsActive = isApproved };
                crowd.AddUser(newUser);

                user = GetUser(username, false);
            }
            catch (CrowdInvalidUserException)
            {
                // User already exists!
                status = MembershipCreateStatus.DuplicateUserName;
                return null;
            }
            catch (CrowdApplicationPermissionException)
            {
                // Application is not allowed to create a new user!
                status = MembershipCreateStatus.ProviderError;
                return null;
            }
            catch (CrowdRestException)
            {
                crowd.RemoveUser(username);
                throw;
            }
            catch
            {
                throw;
            }

            return user ;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                crowd.RemoveUser(username);
            }
            catch (CrowdApplicationPermissionException)
            {
                // Application is not allowed to delete a new user!
                return false;
            }
            catch (CrowdUserNotFoundException)
            {
                // the user could not be found
                return false;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return true;
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (pageIndex < 0)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex"));
            if (pageSize < 1)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageSize"));

            long upperBound = (long)pageIndex * pageSize + pageSize - 1;
            if (upperBound > Int32.MaxValue)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex and pageSize"));

            MembershipUserCollection col = new MembershipUserCollection();
            int lastOffset = (pageIndex + 1) * pageSize;
            int startOffset = lastOffset - pageSize + 1;
            
            try
            {
                PropertyEntity emailProperty = new PropertyEntity{ Name = "email", Type = PropertyType.STRING };
                PropertySearchRestriction searchRestriction = new PropertySearchRestriction{MatchMode = MatchMode.EXACTLY_MATCHES, Value = emailToMatch, Property = emailProperty};
                List<User> userList = crowd.SearchUsers(searchRestriction, 0, -1);

                int count = 0;
                totalRecords = 0;

                foreach (User crowdUser in userList)
                {
                    count++;

                    // 
                    // add only the requested window of the result set
                    // 
                    if (count >= startOffset && count <= lastOffset)
                    {
                        col.Add(GetMembershipUserFromCrowdUser(crowdUser));
                    }
                }
                totalRecords = count;


            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return col;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (pageIndex < 0)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex"));
            if (pageSize < 1)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageSize"));

            long upperBound = (long)pageIndex * pageSize + pageSize - 1;
            if (upperBound > Int32.MaxValue)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex and pageSize"));

            MembershipUserCollection col = new MembershipUserCollection();
            int lastOffset = (pageIndex + 1) * pageSize;
            int startOffset = lastOffset - pageSize + 1;

            try
            {
                PropertyEntity emailProperty = new PropertyEntity { Name = "name", Type = PropertyType.STRING };
                PropertySearchRestriction searchRestriction = new PropertySearchRestriction { MatchMode = MatchMode.EXACTLY_MATCHES, Value = usernameToMatch, Property = emailProperty };
                List<User> userList = crowd.SearchUsers(searchRestriction, 0, -1);

                int count = 0;
                totalRecords = 0;

                foreach (User crowdUser in userList)
                {
                    count++;

                    // add only the requested window of the result set
                    if (count >= startOffset && count <= lastOffset)
                    {
                        col.Add(GetMembershipUserFromCrowdUser(crowdUser));
                    }
                }
                totalRecords = count;


            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return col;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (pageIndex < 0)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex"));
            if (pageSize < 1)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageSize"));

            long upperBound = (long)pageIndex * pageSize + pageSize - 1;
            if (upperBound > Int32.MaxValue)
                throw new ArgumentException(string.Format(exceptionMessageArgumentInvalid, "pageIndex and pageSize"));

            MembershipUserCollection col = new MembershipUserCollection();
            int lastOffset = (pageIndex + 1) * pageSize;
            int startOffset = lastOffset - pageSize + 1;

            try
            {
                // TODO: Check this triggers /crowd/rest/usermanagement/1/search?entity-type=user&expand=user
                List<User> userList = crowd.SearchUsers(0, -1);

                int count = 0;
                totalRecords = 0;

                foreach (User crowdUser in userList)
                {
                    count++;

                    // 
                    // add only the requested window of the result set
                    // 
                    if (count >= startOffset && count <= lastOffset)
                    {
                        col.Add(GetMembershipUserFromCrowdUser(crowdUser));
                    }
                }
                totalRecords = count;


            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return col;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotSupportedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            MembershipUser membershipUser = null; 

            try
            {
                User crowdUser = crowd.GetUser(username);
                if (crowdUser != null)
                {
                    membershipUser = GetMembershipUserFromCrowdUser(crowdUser);
                }
            }
            catch (CrowdUserNotFoundException)
            {
                // the user could not be found
                return null;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return membershipUser;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotSupportedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            string newPassword = null;

            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (!EnablePasswordReset)
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "enablePasswordReset"));

            if (RequiresQuestionAndAnswer && string.IsNullOrEmpty(answer))
                throw new NotSupportedException(string.Format(exceptionMessageArgumentInvalid, "answer"));

            if (!RequiresQuestionAndAnswer && !string.IsNullOrEmpty(answer))
                throw new NotSupportedException(string.Format(exceptionMessageArgumentInvalid, "answer"));

            try
            {
                // get the user's entry
                MembershipUser user = GetUser(username, false);

                // user does not exist, throw exception 
                if (user == null)
                    throw new ProviderException(string.Format(exceptionMessageUserNotFound, username));

                // if user is locked, throw an exception
                if (user.IsLockedOut)
                    throw new MembershipPasswordException(string.Format(exceptionMessageUserLockedOut, username));

                // Reset  the password (generating a random new password)
                newPassword = GeneratePassword();

                ValidatePasswordEventArgs e = new ValidatePasswordEventArgs(username, newPassword, false);
                OnValidatingPassword(e);

                if (e.Cancel)
                {
                    if (e.FailureInformation != null)
                        throw e.FailureInformation;
                    else
                        throw new ProviderException(string.Format(exceptionMessageArgumentInvalid, "newPassword"));
                }

                crowd.UpdateUserPassword(username, newPassword);
            }
            catch
            {
                throw;
            }

            return newPassword;
        }

        public override bool UnlockUser(string username)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);
            try
            {
                // get the user 
                User crowdUser = crowd.GetUser(username);

                if (crowdUser == null)
                    throw new ProviderException(string.Format(exceptionMessageUserNotFound, username));

                crowdUser.IsActive = true;
                crowd.UpdateUser(crowdUser);
            }
            catch (CrowdInvalidUserException)
            {
                // the usernames in the request body and the URI do not match
                return false;
            }
            catch (CrowdApplicationPermissionException)
            {
                // the application is not allowed to update/create a user 
                return false;
            }
            catch (CrowdUserNotFoundException)
            {
                // the user could not be found
                return false;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }
            return true;
        }

        public override void UpdateUser(MembershipUser user)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            if (user == null)
            {
                throw new ArgumentNullException(string.Format(exceptionMessageArgumentInvalid, "user"));
            }
            
            bool isDirty = false;

            try
            {
                CrowdMembershipUser crowdUser = user as CrowdMembershipUser;

                // get the user 
                User existingUser = crowd.GetUser(crowdUser.UserName);

                if (existingUser == null)
                    throw new ProviderException(string.Format(exceptionMessageUserNotFound, user.UserName));

                if (existingUser.Email != crowdUser.Email)
                {
                    existingUser.Email = crowdUser.Email;
                    isDirty = true;
                }
                if (existingUser.IsActive != crowdUser.IsApproved)
                {
                    existingUser.IsActive = crowdUser.IsApproved;
                    isDirty = true;
                }
                if (existingUser.FirstName != crowdUser.FirstName)
                {
                    existingUser.FirstName = crowdUser.FirstName;
                    isDirty = true;
                }
                if (existingUser.LastName != crowdUser.LastName)
                {
                    existingUser.LastName = crowdUser.LastName;
                    isDirty = true;
                }
                if (existingUser.DisplayName != crowdUser.DisplayName)
                {
                    existingUser.DisplayName = crowdUser.DisplayName;
                    isDirty = true;
                } 
                
                if (isDirty)
                {
                    crowd.UpdateUser(existingUser);
                }
            }
            catch (CrowdInvalidUserException)
            {
                // the usernames in the request body and the URI do not match
                throw;
            }
            catch (CrowdApplicationPermissionException)
            {
                // the application is not allowed to update/create a user 
                throw;
            }
            catch (CrowdUserNotFoundException)
            {
                // the user could not be found
                throw;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

        }

        public override bool ValidateUser(string username, string password)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            bool result = false;

            try
            {
                User user = crowd.AuthenticateUser(username, password);
                result = (user != null & user.IsActive);
            }
            catch (CrowdUserNotFoundException)
            {
                return false;
            }
            catch (CrowdInactiveAccountException)
            {
                return false;
            }
            catch (CrowdExpiredCredentialException)
            {
                return false;
            }
            catch (CrowdInvalidAuthenticationException)
            {
                return false;
            }
            catch
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Helper Methods and Properties
        /// <summary>
        /// Gets the configuration value.
        /// </summary>
        /// <param name="configValue">The configuration value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private string GetConfigValue(string configValue, string defaultValue = "")
        {
            //_logger.Debug(string.Format("Getting {0} config value: '{1}'; defaulting to: '{2}'", this.Name, configValue, defaultValue));

            if (string.IsNullOrWhiteSpace(configValue))
                return defaultValue;

            return configValue;
        }

        private MembershipUser GetMembershipUserFromCrowdUser(User user)
        {
            // username
            string username = user.Name;

            // providerUserKey
            object providerUserKey = user.Key;

            // email
            string email = user.Email;

            // comment (not supported)
            string comment = null;

            // passwordQuestion (not supported)
            string passwordQuestion = null;

            //isApproved
            bool isApproved = user.IsActive;

            //isLockedOut (not supported)
            bool isLockedOut = false;

            // creationDate (not supported)
            DateTime creationDate = DateTime.MinValue;


            List<Crowd.Rest.Client.Entity.Attribute> userAttributes = crowd.GetUserAttributes(user.Name);

            //lastLogon
            string lastAuthenticatedValue = GetValueFromUserAttributes(userAttributes, "lastAuthenticated");
            DateTime lastLogon = DateTime.MinValue;
            if (lastAuthenticatedValue != null)
            {
                lastLogon = ConvertJavaTimeStampToDateTime(double.Parse(lastAuthenticatedValue));
            }

            //lastActivity (not supported)
            DateTime lastActivity = DateTime.MinValue;

            //lastPasswordChange
            string lastPasswordChangeValue = GetValueFromUserAttributes(userAttributes, "passwordLastChanged");
            DateTime lastPasswordChange = DateTime.MinValue;
            if (lastPasswordChangeValue != null)
            {
                lastPasswordChange = ConvertJavaTimeStampToDateTime(double.Parse(lastPasswordChangeValue));
            }

            //lastLockoutDate (not supported)
            DateTime lastLockoutDate = DateTime.MinValue;

            // firstName
            string firstName = user.FirstName;

            // lastName
            string lastName = user.LastName;

            // displayName
            string displayName = user.DisplayName;

            // invalidPasswordAttempts
            string invalidPasswordAttemptsValue = GetValueFromUserAttributes(userAttributes, "invalidPasswordAttempts");
            int invalidPasswordAttempts = (string.IsNullOrWhiteSpace(invalidPasswordAttemptsValue) ? 0 : Convert.ToInt32(invalidPasswordAttemptsValue)) ;

            // requiresPasswordChange
            string requiresPasswordChangeValue = GetValueFromUserAttributes(userAttributes, "requiresPasswordChange");
            bool requiresPasswordChange = (string.IsNullOrWhiteSpace(requiresPasswordChangeValue) ? false : Convert.ToBoolean(requiresPasswordChangeValue));

            return new CrowdMembershipUser(Name, username, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLogon, lastActivity, lastPasswordChange, lastLockoutDate, firstName, lastName, displayName, invalidPasswordAttempts, requiresPasswordChange);
        }

        private string GetValueFromUserAttributes(List<Crowd.Rest.Client.Entity.Attribute> attributes, string attributeName)
        {
            Crowd.Rest.Client.Entity.Attribute attribute = attributes.FirstOrDefault(x => x.name == attributeName);
            if (attribute != null)
            {
                return attribute.values.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public virtual string GeneratePassword() 
        { 
            return Membership.GeneratePassword(
                      MinRequiredPasswordLength < PASSWORD_SIZE ? PASSWORD_SIZE : MinRequiredPasswordLength, 
                      MinRequiredNonAlphanumericCharacters); 
        }

        /// <summary>
        /// Converts a Java timestamp to DateTime.
        /// </summary>
        /// <param name="javaTimeStamp">The Java timestamp.</param>
        /// <returns></returns>
        private static DateTime ConvertJavaTimeStampToDateTime(double javaTimeStamp)
        {
            // Java timestamp is millisecods past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(Math.Round(javaTimeStamp / 1000)).ToLocalTime();
            return dtDateTime;
        }
        
        #endregion
    }
}
