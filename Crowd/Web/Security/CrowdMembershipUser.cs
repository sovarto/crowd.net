﻿using System;
using System.Web.Security;

namespace Crowd.Web.Security
{
    /// <summary>
    /// Test
    /// </summary>
    public class CrowdMembershipUser : MembershipUser
    {
        #region MembershipUser Fields and Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public int InvalidPasswordAttempts { get; set; }
        public bool RequiresPasswordChange { get; set; }
        #endregion

        #region Constructors
        public CrowdMembershipUser(string providerName, 
                                string              name, 
                                object              providerUserKey,
                                string              email, 
                                string              passwordQuestion,
                                string              comment,
                                bool                isApproved,
                                bool                isLockedOut, 
                                DateTime            creationDate,
                                DateTime            lastLoginDate, 
                                DateTime            lastActivityDate, 
                                DateTime            lastPasswordChangedDate,
                                DateTime            lastLockoutDate,
                                string              firstName,
                                string              lastName,
                                string              displayName,
                                int                 invalidPasswordAttempts,
                                bool                requiresPasswordChange) 
            :base(providerName,
                        name,
                        providerUserKey,
                        email, 
                        passwordQuestion,
                        comment, 
                        isApproved, 
                        isLockedOut,
                        creationDate, 
                        lastLoginDate,
                        lastActivityDate,
                        lastPasswordChangedDate,
                        lastLockoutDate) 
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.DisplayName = displayName;
            this.InvalidPasswordAttempts = invalidPasswordAttempts;
            this.RequiresPasswordChange = requiresPasswordChange;
        } 

        protected CrowdMembershipUser() { } // Default CTor: Callable by derived class only.
        #endregion

    } 
}
